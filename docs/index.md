# Home

## Introduction
Gothic 2 Online (**G2O**) is a free multiplayer modification for popular RPG _"Gothic II: Night of the Raven 2.6_". Originally founded in early 2016 as an replacement for almost dead GMP/GMP:A. Since then growing into advanced multiplayer platform.

Multiplayer was build using reverse engineering techniques with own SDK, later changed to AST. Gothic 2 Online exposes original game's engine functionality via a scripting language "_Squirrel_". Using scripts a minimal sandbox style game, can be changed to any game mode. Scripting API can be extended by external modules. Both server and client supports scripting and modules.

The project is developed and maintained by **G2O Team** and remains closed-source for security reasons.

The modification files can be download through our [website](https://gothic-online.com.pl).

Check for more information about scripting language.  
- [Official documentation](http://squirrel-lang.org/squirreldoc/)  
- [Electric Imp introduction/reference](https://developer.electricimp.com/squirrel/squirrelcrib)  

Looking for help or scripting resources? Join to our scripting community discord server.  
- [Gothic 2 Online Scripting Community](https://discord.gg/bjtC7GwQSF)  

Browse our official and community repositories.  
- [Official projects](https://gitlab.com/GothicMultiplayerTeam)  
- [Community projects](https://gitlab.com/g2o)  
