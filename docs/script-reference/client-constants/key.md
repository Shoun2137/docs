---
title: 'Key'
---
# `constants` Key <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `KEY_ESCAPE` | ESC |
| `KEY_1` | 1 |
| `KEY_2` | 2 |
| `KEY_3` | 3 |
| `KEY_4` | 4 |
| `KEY_5` | 5 |
| `KEY_6` | 6 |
| `KEY_7` | 7 |
| `KEY_8` | 8 |
| `KEY_9` | 9 |
| `KEY_0` | 0 |
| `KEY_MINUS` | - |
| `KEY_EQUALS` | = |
| `KEY_BACK` | BACKSPACE |
| `KEY_TAB` | TAB |
| `KEY_Q` | Q |
| `KEY_W` | W |
| `KEY_E` | E |
| `KEY_R` | R |
| `KEY_T` | T |
| `KEY_Y` | Y |
| `KEY_U` | U |
| `KEY_I` | I |
| `KEY_O` | O |
| `KEY_P` | P |
| `KEY_LBRACKET` | [ |
| `KEY_RBRACKET` | ] |
| `KEY_RETURN` | ENTER |
| `KEY_LCONTROL` | L_CTRL |
| `KEY_A` | A |
| `KEY_S` | S |
| `KEY_D` | D |
| `KEY_F` | F |
| `KEY_G` | G |
| `KEY_H` | H |
| `KEY_J` | J |
| `KEY_K` | K |
| `KEY_L` | L |
| `KEY_SEMICOLON` | ; |
| `KEY_APOSTROPHE` | ' |
| `KEY_TILDE` | ~ |
| `KEY_LSHIFT` | L_SHIFT |
| `KEY_BACKSLASH` | \ |
| `KEY_Z` | Z |
| `KEY_X` | X |
| `KEY_C` | C |
| `KEY_V` | V |
| `KEY_B` | B |
| `KEY_N` | N |
| `KEY_M` | M |
| `KEY_COMMA` | , |
| `KEY_PERIOD` | . |
| `KEY_SLASH` | / |
| `KEY_RSHIFT` | R_SHIFT |
| `KEY_MULTIPLY` | * |
| `KEY_LMENU` | L_ALT |
| `KEY_SPACE` | SPACE |
| `KEY_CAPITAL` | CAPSLOCK |
| `KEY_F1` | F1 |
| `KEY_F2` | F2 |
| `KEY_F3` | F3 |
| `KEY_F4` | F4 |
| `KEY_F5` | F5 |
| `KEY_F6` | F6 |
| `KEY_F7` | F7 |
| `KEY_F8` | F8 |
| `KEY_F9` | F9 |
| `KEY_F10` | F10 |
| `KEY_NUMLOCK` | NUMLOCK |
| `KEY_SCROLL` | SCROLLLOCK |
| `KEY_NUMPAD7` | NUMPAD7 |
| `KEY_NUMPAD8` | NUMPAD8 |
| `KEY_NUMPAD9` | NUMPAD9 |
| `KEY_SUBTRACT` | - |
| `KEY_NUMPAD4` | NUMPAD4 |
| `KEY_NUMPAD5` | NUMPAD5 |
| `KEY_NUMPAD6` | NUMPAD6 |
| `KEY_ADD` | + |
| `KEY_NUMPAD1` | NUMPAD1 |
| `KEY_NUMPAD2` | NUMPAD2 |
| `KEY_NUMPAD3` | NUMPAD3 |
| `KEY_NUMPAD0` | NUMPAD0 |
| `KEY_DECIMAL` | NUM . |
| `KEY_F11` | F11 |
| `KEY_F12` | F12 |
| `KEY_NUMPADENTER` | NUM ENTER |
| `KEY_RCONTROL` | R_CTRL |
| `KEY_DIVIDE` | / |
| `KEY_RMENU` | R_ALT |
| `KEY_PAUSE` | Pause/Break |
| `KEY_HOME` | Home |
| `KEY_UP` | ARROW UP |
| `KEY_PRIOR` | Page Up |
| `KEY_LEFT` | ARROW LEFT |
| `KEY_RIGHT` | ARROW RIGHT |
| `KEY_END` | END |
| `KEY_DOWN` | ARROW DOWN |
| `KEY_NEXT` | Page Down |
| `KEY_INSERT` | INSERT |
| `KEY_DELETE` | DELETE |
| `KEY_LWIN` | L_WINDOWS |
| `KEY_RWIN` | R_WINDOWS |
| `KEY_POWER` | POWER |
| `KEY_SLEEP` | SLEEP |
| `KEY_WAKE` | WAKE |
