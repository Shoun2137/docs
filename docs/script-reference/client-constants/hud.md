---
title: 'HUD'
---
# `constants` HUD <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `HUD_ALL` | Represents all hud elements |
| `HUD_HEALTH_BAR` | Represents game health bar |
| `HUD_MANA_BAR` | Represents game mana bar |
| `HUD_SWIM_BAR` | Represents game swim bar (oxygen) |
| `HUD_FOCUS_BAR` | Represents game focus bar (focused npc health bar) |
| `HUD_FOCUS_NAME` | Represents game focus name (text displayed above focused object) |
| `HUD_MODE_HIDDEN` | Represents hidden hud mode (elements having this mode won't be visible on screen) |
| `HUD_MODE_DEFAULT` | Represents default hud mode (elements having this mode will be displayed when game shows them, e.g: swim bar will be visible only while diving) |
| `HUD_MODE_ALWAYS_VISIBLE` | Represents always visible hud mode (elements having this mode will be always visible on screen) |
