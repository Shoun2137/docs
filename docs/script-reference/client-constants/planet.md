---
title: 'Planet'
---
# `constants` Planet <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `PLANET_SUN` | Represents planet sun type. |
| `PLANET_MOON` | Represents planet moon type. |
