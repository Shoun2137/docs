---
title: 'TraceRay'
---
# `constants` TraceRay <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `TRACERAY_VOB_IGNORE` | Represents traceray flag that ignores vobs. |
| `TRACERAY_VOB_IGNORE_NO_CD_DYN` | Represents traceray flag that ignores vobs, if they have not enabled dynamic collision. |
| `TRACERAY_VOB_IGNORE_CHARACTER` | Represents traceray flag that ignores vobs that are characters. |
| `TRACERAY_VOB_IGNORE_PROJECTILES` | Represents traceray flag that ignores projectile vobs, e.g: magic, arrows and bolts. |
| `TRACERAY_VOB_BBOX` | Represents traceray flag that checks for vobs with bounding box. |
| `TRACERAY_STAT_IGNORE` | Represents traceray flag that ignores static world geometry. |
| `TRACERAY_STAT_POLY` | Represents traceray flag that checks for static world polygons. |
| `TRACERAY_STAT_PORTALS` | Represents traceray flag that checks for static world portals. |
| `TRACERAY_POLY_NORMAL` | Represents traceray flag that checks for polygons of world and vobs. |
| `TRACERAY_POLY_IGNORE_TRANSP` | Represents traceray flag that ignores transparent polygons. |
| `TRACERAY_POLY_TEST_WATER` | Represents traceray flag that checks for polygons marked as water. |
| `TRACERAY_POLY_2SIDED` | Represents traceray flag that checks for double-sided polygons. |
