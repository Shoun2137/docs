---
title: 'DaedalusType'
---
# `constants` DaedalusType <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `DAEDALUS_TYPE_VOID` | Represents void type in daedalus scripting language (e.g: function that doesn't return). |
| `DAEDALUS_TYPE_FLOAT` | Represents float type in daedalus scripting language. |
| `DAEDALUS_TYPE_INT` | Represents integer type in daedalus scripting language. |
| `DAEDALUS_TYPE_STRING` | Represents string type in daedalus scripting language. |
| `DAEDALUS_TYPE_CLASS` | Represents class type in daedalus scripting language. |
| `DAEDALUS_TYPE_FUNC` | Represents func type in daedalus scripting language. |
| `DAEDALUS_TYPE_PROTOTYPE` | Represents prototype type in daedalus scripting language. |
| `DAEDALUS_TYPE_INSTANCE` | Represents instance type in daedalus scripting language. |
