---
title: 'Hero Status'
---
# `constants` Hero Status <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `HERO_STATUS_STD` | Represents standard hero status (active music theme with _STD suffix). |
| `HERO_STATUS_THR` | Represents threat hero status (active music theme with _THR suffix). |
| `HERO_STATUS_FGT` | Represents fight hero status (active music theme with _FGT suffix). |
