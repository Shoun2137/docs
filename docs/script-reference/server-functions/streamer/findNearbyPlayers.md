---
title: 'findNearbyPlayers'
---
# `function` findNearbyPlayers <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This function will search for nearest players, that matches given query arguments.

## Declaration
```cpp
array[int] findNearbyPlayers(Vec3 position, int radius, string world, int virtual_world = 0)
```

## Parameters
* `Vec3` **position**: the centroid position.
* `int` **radius**: the maximum radius to search from centroid.
* `string` **world**: the world used to find players.
* `int` **virtual_world**: the virtual world used to find players.
  
## Returns `array[int]`
ids of nearby players.

