---
title: 'getStreamedPlayersByPlayer'
---
# `function` getStreamedPlayersByPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to retrieve currently streamed players by given player.
More details:
Streamed players are basically clients, that has spawned given player in their game.
Please notice, that player can be spawned only one way. Which means that there are situation were player 1 is spawned for player 2, but not the other way arount.
Simple examples:
- Invisible players cannot be seen, but they can see everyone nearby.
- Flying around world using camera.

## Declaration
```cpp
array[int] getStreamedPlayersByPlayer(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `array[int]`
ids of streamed players.

