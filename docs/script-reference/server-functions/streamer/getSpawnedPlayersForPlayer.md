---
title: 'getSpawnedPlayersForPlayer'
---
# `function` getSpawnedPlayersForPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to retrieve currently spawned players for given player.

## Declaration
```cpp
array[int] getSpawnedPlayersForPlayer(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `array[int]`
ids of spawned players.

