---
title: 'getPlayerVirtualWorld'
---
# `function` getPlayerVirtualWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.2"

This function will get the player virtual world.

## Declaration
```cpp
int getPlayerVirtualWorld(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the player virtual world id.

