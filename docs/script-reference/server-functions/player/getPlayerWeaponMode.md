---
title: 'getPlayerWeaponMode'
---
# `function` getPlayerWeaponMode <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player weapon mode.

## Declaration
```cpp
int getPlayerWeaponMode(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the player weaponMode, for more information see [Weapon mode constants](../../../shared-constants/weapon-mode/).

