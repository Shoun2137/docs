---
title: 'isPlayerSpawned'
---
# `function` isPlayerSpawned <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether player is spawned.

## Declaration
```cpp
bool isPlayerSpawned(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool`
`true` when player is spawned, otherwise `false`.

