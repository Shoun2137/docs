---
title: 'getPlayerMana'
---
# `function` getPlayerMana <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"

This function will get the player mana points.

## Declaration
```cpp
int getPlayerMana(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the mana points amount.

