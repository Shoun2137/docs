---
title: 'getPlayerAniId'
---
# `function` getPlayerAniId <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the active player animation id.

## Declaration
```cpp
int getPlayerAniId(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the ani id.

