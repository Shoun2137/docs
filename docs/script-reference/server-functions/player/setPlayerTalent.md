---
title: 'setPlayerTalent'
---
# `function` setPlayerTalent <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will toggle the player talent for all players.

## Declaration
```cpp
void setPlayerTalent(int id, int talentId, bool toggle)
```

## Parameters
* `int` **id**: the player id.
* `int` **talentId**: the talent id. For more information see [Talent constants](../../../shared-constants/talent/).
* `bool` **toggle**: `true` if talent should be enabled for player, otherwise `false`.
  

