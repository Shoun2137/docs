---
title: 'getPlayerHealth'
---
# `function` getPlayerHealth <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player health points.

## Declaration
```cpp
int getPlayerHealth(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the health points amount.

