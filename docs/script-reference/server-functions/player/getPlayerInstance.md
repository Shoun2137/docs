---
title: 'getPlayerInstance'
---
# `function` getPlayerInstance <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player instance.

## Declaration
```cpp
string getPlayerInstance(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player instance.

