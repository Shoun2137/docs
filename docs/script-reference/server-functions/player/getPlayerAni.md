---
title: 'getPlayerAni'
---
# `function` getPlayerAni <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will get the active player animation.

## Declaration
```cpp
string getPlayerAni(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the ani name, e.g: `"S_RUN"`.

