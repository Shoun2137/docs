---
title: 'getPlayerScale'
---
# `function` getPlayerScale <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player scale.

## Declaration
```cpp
{x, y, z} getPlayerScale(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `{x, y, z}`
the player scale.

