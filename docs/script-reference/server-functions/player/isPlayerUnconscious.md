---
title: 'isPlayerUnconscious'
---
# `function` isPlayerUnconscious <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether player is in unconscious state.
The player will be unconscious, when it gets beaten up, but not killed.

## Declaration
```cpp
bool isPlayerUnconscious(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool`
`true` when player is unconscious, otherwise `false`.

