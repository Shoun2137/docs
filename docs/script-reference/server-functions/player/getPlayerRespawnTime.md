---
title: 'getPlayerRespawnTime'
---
# `function` getPlayerRespawnTime <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.2"

This function will get the player time to respawn after death.

## Declaration
```cpp
string getPlayerRespawnTime(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player respawn time.

