---
title: 'hitPlayer'
---
# `function` hitPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function is used to simulate hit between attacker and victim.
It will only work with if killer or victim is a real player. The victim will receive damage calculated damage by the game.

## Declaration
```cpp
bool hitPlayer(int id, int id)
```

## Parameters
* `int` **id**: the attacker id.
* `int` **id**: the victim id.
  
## Returns `bool`
`true` if hit was successfully simulated, otherwise `false`.

