---
title: 'getPlayerFocus'
---
# `function` getPlayerFocus <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function is used to get current focused player by other player.

## Declaration
```cpp
int getPlayerFocus(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the current focused player id. In case were there is no focus returns `-1`.

