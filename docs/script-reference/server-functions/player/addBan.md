---
title: 'addBan'
---
# `function` addBan <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    All properties should be of primitive types and are optional, but you still need to provide at least one of them.
!!! note
    The reason string can't be longer than 255 characters.
!!! note
    The meta table is used for storing custom data.

This function will add a new ban on the server.

## Declaration
```cpp
bool addBan({ serial, mac, ip, name, reason, timestamp, meta={..} } info)
```

## Parameters
* `{ serial, mac, ip, name, reason, timestamp, meta={..} }` **info**: the ban properties.
  
## Returns `bool`
`true` if ban was added, otherwise `false`.

