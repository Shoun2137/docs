---
title: 'getPlayerColor'
---
# `function` getPlayerColor <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player nickname color.

## Declaration
```cpp
Color getPlayerColor(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Color`
the player nickname color.

