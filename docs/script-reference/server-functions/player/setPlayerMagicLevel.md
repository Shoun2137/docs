---
title: 'setPlayerMagicLevel'
---
# `function` setPlayerMagicLevel <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"

This function will set the player magic level for all players.

## Declaration
```cpp
void setPlayerMagicLevel(int id, int magicLevel)
```

## Parameters
* `int` **id**: the player id.
* `int` **magicLevel**: the magic level.
  

