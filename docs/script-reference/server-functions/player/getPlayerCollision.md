---
title: 'getPlayerCollision'
---
# `function` getPlayerCollision <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This function will get the player collision.

## Declaration
```cpp
bool getPlayerCollision(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool`
`true` if collision is enabled, otherwise `false`.

