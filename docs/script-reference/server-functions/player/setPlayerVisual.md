---
title: 'setPlayerVisual'
---
# `function` setPlayerVisual <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player visual for all players.

## Declaration
```cpp
void setPlayerVisual(int id, string bodyModel, int bodyTxt, string headModel, int headTxt)
```

## Parameters
* `int` **id**: the player id.
* `string` **bodyModel**: the name of the body model (ASC), e.g: `HUM_BODY_NAKED0`.
* `int` **bodyTxt**: the numeric id of body texture file. Texture id can be read from `V(number)` filename part, for example, in this file: `HUM_BODY_NAKED_V8_C0-C.TEX` id is 8.
* `string` **headModel**: the name of the head model (MMS), e.g: `HUM_HEAD_PONY`.
* `int` **headTxt**: the numeric id of head texture file. Texture id can be read from `V(number)` filename part, for example, in this file: `HUM_HEAD_V18_C0-C.TEX` id is 18.
  

