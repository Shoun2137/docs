---
title: 'setServerWorld'
---
# `function` setServerWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.0"
!!! note
    The server world limit is set to 32 characters.
!!! note
    If the target world path is written with backslashes instead of normal slashes, you need to escape it with another backslashes e.g. "NEWWORLD\\NEWWORLD.ZEN".

This function will change the default world to which players will enter after joining.

## Declaration
```cpp
void setServerWorld(string world)
```

## Parameters
* `string` **world**: the path to the target world.
  

