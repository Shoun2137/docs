---
title: 'enableEvent_onTick'
---
# `function` enableEvent_onTick <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    By default this event is disabled.

This function will enable/disable triggering of [onTick](../../../server-events/general/onTick/) event.

## Declaration
```cpp
void enableEvent_onTick(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to enable triggering of this event, otherwise `false`.
  

