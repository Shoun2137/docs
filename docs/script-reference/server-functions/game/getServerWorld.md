---
title: 'getServerWorld'
---
# `function` getServerWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.0"

The function is used to get the path of the default world on the server.

## Declaration
```cpp
string getServerWorld()
```

## Parameters
No parameters.
  
## Returns `string`
The world path name.

