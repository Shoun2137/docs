---
title: 'setDayLength'
---
# `function` setDayLength <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    day length can't be smaller than 10 000 miliseconds.

This function will set the day length in miliseconds.

## Declaration
```cpp
void setDayLength(float miliseconds)
```

## Parameters
* `float` **miliseconds**: day length in miliseconds.
  

