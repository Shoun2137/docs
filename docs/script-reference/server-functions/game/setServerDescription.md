---
title: 'setServerDescription'
---
# `function` setServerDescription <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.0"
!!! note
    The server description limit is set to 400 characters.

This function will set the description of the server.

## Declaration
```cpp
bool setServerDescription(string description)
```

## Parameters
* `string` **description**: the server description.
  
## Returns `bool`
`true` if server description was set successfully, otherwise `false`.

