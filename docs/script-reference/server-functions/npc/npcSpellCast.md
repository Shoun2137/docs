---
title: 'npcSpellCast'
---
# `function` npcSpellCast <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Required equiped and setup magic.

This function enqueue spell cast command for remote NPC.

## Declaration
```cpp
void npcSpellCast(int attacker_id, int enemy_id)
```

## Parameters
* `int` **attacker_id**: the remote npc id.
* `int` **enemy_id**: the remote npc or player id.
  

