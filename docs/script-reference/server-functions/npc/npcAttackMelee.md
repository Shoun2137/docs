---
title: 'npcAttackMelee'
---
# `function` npcAttackMelee <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Combo is internal Gothic value. Its behaviour can be sometimes undefined. For example -1 value doesn't work for not humanoid NPCs.

This function enqueue attack melee command for remote NPC.

## Declaration
```cpp
void npcAttackMelee(int attacker_id, int enemy_id, int type, int combo)
```

## Parameters
* `int` **attacker_id**: the remote npc id.
* `int` **enemy_id**: the remote npc or player id.
* `int` **type**: the type of attack (0 - running hit, 1 - forward hit, 2 - left hit, 3 - right hit).
* `int` **combo**: the combo sequence. For -1 execute next command immediately.
  

