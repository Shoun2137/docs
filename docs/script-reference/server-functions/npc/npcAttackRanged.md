---
title: 'npcAttackRanged'
---
# `function` npcAttackRanged <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Required equiped and drawn bow/crossbow.

This function enqueue attack ranged command for remote NPC.

## Declaration
```cpp
void npcAttackRanged(int attacker_id, int enemy_id)
```

## Parameters
* `int` **attacker_id**: the remote npc id.
* `int` **enemy_id**: the remote npc or player id.
  

