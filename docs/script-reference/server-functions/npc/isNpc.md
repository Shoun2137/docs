---
title: 'isNpc'
---
# `function` isNpc <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function checks whether id related to given object is remote NPC.

## Declaration
```cpp
bool isNpc(int id)
```

## Parameters
* `int` **id**: the npc id.
  
## Returns `bool`
`true` when object is NPC, otherwise `false`.

