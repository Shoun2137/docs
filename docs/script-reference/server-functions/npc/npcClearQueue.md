---
title: 'npcClearQueue'
---
# `function` npcClearQueue <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function clears remote NPC command queue.
Remote NPCs uses command queue to execute thier tasks.

## Declaration
```cpp
void npcClearQueue(int id)
```

## Parameters
* `int` **id**: the npc id.
  

