---
title: 'getNpcHostPlayer'
---
# `function` getNpcHostPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function returns NPC host player id.

## Declaration
```cpp
int getNpcHostPlayer(int id)
```

## Parameters
* `int` **id**: the npc id.
  
## Returns `int`
the host player id. If there is no host player -1 is returned instead.

