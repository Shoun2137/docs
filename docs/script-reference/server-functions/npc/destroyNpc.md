---
title: 'destroyNpc'
---
# `function` destroyNpc <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function destroys remote NPC.

## Declaration
```cpp
bool destroyNpc(int npcId)
```

## Parameters
* `int` **npcId**: the id of npc.
  
## Returns `bool`
`true` when npc was successfully destroyed, otherwise `false`.

