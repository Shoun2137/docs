---
title: 'getWaypoint'
---
# `function` getWaypoint <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.0"

This function is used to retrieve the position of specified waypoint.

## Declaration
```cpp
{x, y, z} getWaypoint(string world, string name)
```

## Parameters
* `string` **world**: the world name in which the waypoint exists.
* `string` **name**: the name of the waypoint.
  
## Returns `{x, y, z}`
The position of waypoint.

