---
title: 'sendPlayerMessageToAll'
---
# `function` sendPlayerMessageToAll <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.0"

This function will send a chat message from one player to every player.
Sending a message triggers client side event [onPlayerMessage](../../../client-events/player/onPlayerMessage/) with playerid set as **senderid**.

## Declaration
```cpp
void sendPlayerMessageToAll(int senderid, int r, int g, int b, string text)
```

## Parameters
* `int` **senderid**: the id of the player which will send a message.
* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
* `string` **text**: that will be send.
  

