---
title: 'Daedalus'
---
# `static class` Daedalus <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This class represents Daedalus scripting interface.


## Properties
No properties.

----

## Methods
### index

This method will get the daedalus symbol index by its name.

```cpp
int index(string name)
```

**Parameters:**

* `string` **name**: the name of the daedalus symbol.
  
**Returns `int`:**

the daedalus symbol index number.

----
### symbol

This method will get the daedalus symbol by its name.

```cpp
{name, index}|null symbol(string name)
```

**Parameters:**

* `string` **name**: the name of the daedalus symbol.
  
**Returns `{name, index}|null`:**

the daedalus symbol or null.

----
### instance

This method will get the all of the daedalus instance variables.

```cpp
table instance(string instanceName)
```

**Parameters:**

* `string` **instanceName**: the name of the daedalus instance.
  
**Returns `table`:**

the object containing all of the daedalus instance variables.

----
