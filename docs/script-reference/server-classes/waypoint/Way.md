---
title: 'Way'
---
# `class` Way <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.4"

This class represents Way constructed from waypoints.

### Constructor
```cpp
Way(string world, string startWp, string endWp)
```

**Parameters:**

* `string` **world**: the name of the world from config.xml.
* `string` **startWp**: the name of the start waypoint.
* `string` **endWp**: the name of the end waypoint.

## Properties
### `string` start <font size="2">(read-only)</font>

Represents the start waypoint for Way.

----
### `string` end <font size="2">(read-only)</font>

Represents the end waypoint for Way.

----

## Methods
### getWaypoints

This method will get the all waypoints between startWp & endWp.

```cpp
[wpName...] getWaypoints()
```

  
**Returns `[wpName...]`:**

the array containing the names of all of the Way waypoints.

----
### getCountWaypoints

This method will get the number of waypoints between start waypoint & end waypoint.

```cpp
int getCountWaypoints()
```

  
**Returns `int`:**

the number of waypoints.

----
