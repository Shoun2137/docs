---
title: 'Packet'
---
# `class` Packet <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.3"

This class represents data packet that gets send over the network.


## Properties
### `int` bitsUsed 
!!! info "Available since version: 0.3.0"

Represents the total number of bits used by the script packet data.

----
### `int` bytesUsed 
!!! info "Available since version: 0.3.0"

Represents the total number of bytes used by the script packet data.

----

## Methods
### reset

This method will clear the packet data, making it empty.

```cpp
void reset()
```

  

----
### send

This method will send the packet data to the specified player.

```cpp
void send(int playerid, int Reliability)
```

**Parameters:**

* `int` **playerid**: the id of the player to whom you want to send the packet.
* `int` **Reliability**: the reliability type, for more information see [Reliability constants](../../../shared-constants/reliability).
  

----
### sendToAll

This method will send the packet data to all of the connected players.

```cpp
void sendToAll(int Reliability)
```

**Parameters:**

* `int` **Reliability**: the reliability type, for more information see [Reliability constants](../../../shared-constants/reliability).
  

----
### writeBool

This method will append boolean value to the packet. (1 bit)

```cpp
void writeBool(bool value)
```

**Parameters:**

* `bool` **value**: the boolean value.
  

----
### writeInt8
!!! info "Available since version: 0.1.0"

This method will append signed int8 value to the packet. (1 byte)

```cpp
void writeInt8(int value)
```

**Parameters:**

* `int` **value**: the number value in range -128 to 127.
  

----
### writeUInt8
!!! info "Available since version: 0.1.0"

This method will append unsigned int8 value to the packet. (1 byte)

```cpp
void writeUInt8(int value)
```

**Parameters:**

* `int` **value**: the number value in range 0 to 255.
  

----
### writeInt16

This method will append signed int16 value to the packet. (2 bytes)

```cpp
void writeInt16(int value)
```

**Parameters:**

* `int` **value**: the number value in range -32768 to 32767.
  

----
### writeUInt16
!!! info "Available since version: 0.1.0"

This method will append unsigned int16 value to the packet. (2 bytes)

```cpp
void writeUInt16(int value)
```

**Parameters:**

* `int` **value**: the number value in range 0 to 65535.
  

----
### writeInt32

This method will append signed int32 value to the packet. (4 bytes)

```cpp
void writeInt32(int value)
```

**Parameters:**

* `int` **value**: the number value in range -2147483648 to 2147483647.
  

----
### writeUInt32
!!! info "Available since version: 0.1.0"
!!! note
    By default squirrel uses int32 values, which means that this method behaves exactly the same as [writeInt32](#writeint32) in scripts.

This method will append unsigned int32 value to the packet. (4 bytes)

```cpp
void writeUInt32(int value)
```

**Parameters:**

* `int` **value**: the number value in range 0 to 4294967295.
  

----
### writeFloat

This method will append float value to the packet. (4 bytes)

```cpp
void writeFloat(float value)
```

**Parameters:**

* `float` **value**: the number value.
  

----
### writeString
!!! note
    The amount of bytes appended to the packet depends on the length of string, 1 byte = 1 char.

This method will append string value to the packet.

```cpp
void writeString(string value)
```

**Parameters:**

* `string` **value**: the text value.
  

----
### readBool

This method will get boolean value from the packet.

```cpp
bool readBool()
```

  
**Returns `bool`:**

the boolean value.

----
### readInt8
!!! info "Available since version: 0.1.0"

This method will get signed int8 value from the packet. (1 byte)

```cpp
int readInt8()
```

  
**Returns `int`:**

the number value in range -128 to 127.

----
### readUInt8
!!! info "Available since version: 0.1.0"

This method will get unsigned int8 value from the packet. (1 byte)

```cpp
int readUInt8()
```

  
**Returns `int`:**

the number value in range 0 to 255.

----
### readInt16

This method will get signed int16 value from the packet. (2 bytes)

```cpp
int readInt16()
```

  
**Returns `int`:**

the number value in range -32768 to 32767.

----
### readUInt16
!!! info "Available since version: 0.1.0"

This method will get unsigned int16 value from the packet. (2 bytes)

```cpp
int readUInt16()
```

  
**Returns `int`:**

the number value in range 0 to 65535.

----
### readInt32

This method will get signed int32 value from the packet. (4 bytes)

```cpp
int readInt32()
```

  
**Returns `int`:**

the number value in range -2147483648 to 2147483647.

----
### readUInt32
!!! info "Available since version: 0.1.0"

This method will get unsigned int32 value from the packet. (4 bytes)

```cpp
int readUInt32()
```

  
**Returns `int`:**

the number value in range 0 to 4294967295.

----
### readFloat

This method will get float value from the packet. (4 bytes)

```cpp
float readFloat()
```

  
**Returns `float`:**

the number value.

----
### readString
!!! note
    The amount of bytes appended to the packet depends on the length of string, 1 byte = 1 char.

This method will get string value from the packet.

```cpp
string readString()
```

  
**Returns `string`:**

the text value.

----
