---
title: 'ItemsGround'
---
# `static class` ItemsGround <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.0"

This class represents item ground manager.


## Properties
No properties.

----

## Methods
### getById
!!! info "Available since version: 0.3.0"

This method will retrieve the item ground object by its unique id.

```cpp
ItemGround getById(int itemGroundId)
```

**Parameters:**

* `int` **itemGroundId**: the unique item ground id.
  
**Returns `ItemGround`:**

the item ground object or `null` if the object cannot be found.

----
### create
!!! info "Available since version: 0.3.0"

This method will create the item ground.

```cpp
int create({instance, amount=1, physicsEnabled=false position={x=0,y=0,z=0}, rotation={x=0,y=0,z=0}, world=CONFIG_WORLD, virtualWorld=0}  arg)
```

**Parameters:**

* `{instance, amount=1, physicsEnabled=false position={x=0,y=0,z=0}, rotation={x=0,y=0,z=0}, world=CONFIG_WORLD, virtualWorld=0} ` **arg**: <ul> <li>`string` **arg.instance**: the scripting instance of game item.</li> <li>`int` **arg.amount**: the item amount of the item ground.</li> <li>`bool` **arg.physicsEnabled**: the physics state of the item ground.</li> <li>`table` **arg.position**: the position of the item ground in the world.</li> <li>`table` **arg.rotation**: the rotation of the item ground in the world.</li> <li>`string` **arg.world**: the world the item ground is in (.ZEN file path).</li> <li>`int` **arg.virtualWorld**: the virtual world id in range <0, 65535>.</li> </ul>
  
**Returns `int`:**

the item ground id.

----
### destroy

This method will destroy the item ground by it's unique id.

```cpp
void destroy(int itemGroundId)
```

**Parameters:**

* `int` **itemGroundId**: the item ground unique id.
  

----
