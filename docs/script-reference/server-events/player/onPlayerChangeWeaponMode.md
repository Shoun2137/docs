---
title: 'onPlayerChangeWeaponMode'
---
# `event` onPlayerChangeWeaponMode <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player changes the weapon mode.

## Parameters
```c++
int playerid, int oldWeaponMode, int newWeaponMode
```

* `int` **playerid**: the id of the player which changes the weapon mode.
* `int` **oldWeaponMode**: the old weapon mode which was used by the player. For more information see [Weapon mode constants](../../../shared-constants/weapon-mode/).
* `int` **newWeaponMode**: the new weapon mode in which player is currently using. For more information see [Weapon mode constants](../../../shared-constants/weapon-mode/).

