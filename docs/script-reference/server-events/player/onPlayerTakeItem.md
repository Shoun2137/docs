---
title: 'onPlayerTakeItem'
---
# `event` onPlayerTakeItem <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.0"
!!! tip "This event can be canceled"
!!! note
    Even if this event is triggered it doesn't mean, that player will get item to his inventory. It only means, that the player tried to get the item from the ground. Server is the last decide if the item can be taken from the ground. Canceling this event will prevent the item to be taken from the ground.

This event is triggered when player takes an item from the ground.

## Parameters
```c++
int playerid, ItemGround itemGround
```

* `int` **playerid**: the id of the player who tries to take the ground item.
* `ItemGround` **itemGround**: the ground item object which player tried to to take.

