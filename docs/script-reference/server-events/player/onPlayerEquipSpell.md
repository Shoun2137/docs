---
title: 'onPlayerEquipSpell'
---
# `event` onPlayerEquipSpell <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This event is triggered when player equips or unequips scroll or rune. When item is unequiped, `null` is returned instead.

## Parameters
```c++
int playerid, int slotId, string|null instance
```

* `int` **playerid**: the id of the player who equips a spell.
* `int` **slotId**: the slot id that the player puts the spell on in range <0, 6>.
* `string|null` **instance**: the item instance from Daedalus scripts.

