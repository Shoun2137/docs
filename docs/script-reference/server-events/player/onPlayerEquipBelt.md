---
title: 'onPlayerEquipBelt'
---
# `event` onPlayerEquipBelt <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This event is triggered when player equips or unequips belt. When item is unequiped, `null` is returned instead.

## Parameters
```c++
int playerid, string|null instance
```

* `int` **playerid**: the id of the player who equips a belt.
* `string|null` **instance**: the item instance from Daedalus scripts.

