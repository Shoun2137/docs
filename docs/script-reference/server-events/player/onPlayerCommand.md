---
title: 'onPlayerCommand'
---
# `event` onPlayerCommand <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when a player uses command on the chat.  
Command always begins with forward slash `/`.

## Parameters
```c++
int playerid, string command, string params
```

* `int` **playerid**: the id of the player who typed the command.
* `string` **command**: used command name on the chat.
* `string` **params**: command parameters divided by space.

