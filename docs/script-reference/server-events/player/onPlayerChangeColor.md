---
title: 'onPlayerChangeColor'
---
# `event` onPlayerChangeColor <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player nickname color was changed for all players.

## Parameters
```c++
int playerid, int r, int g, int b
```

* `int` **playerid**: the id of the player whose nickname color was changed.
* `int` **r**: the amount of red in the nickname color `(0 - 255)`.
* `int` **g**: the amount of green in the nickname color `(0 - 255)`.
* `int` **b**: the amount of blue in the nickname color `(0 - 255)`.

