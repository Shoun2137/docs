---
title: 'onPlayerEquipHandItem'
---
# `event` onPlayerEquipHandItem <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when game adds item to player hand, e.g: when player opens or consumes any item.
When item is removed from hand, `null` is returned instead.

## Parameters
```c++
int playerid, int hand, string|null instance
```

* `int` **playerid**: the id of the player who gets an item to his hand.
* `int` **hand**: the id of the hand in which player holds item.  For more information see [Hand constants](../../../shared-constants/hand/).
* `string|null` **instance**: the item instance from Daedalus scripts.

