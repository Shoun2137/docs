---
title: 'onPlayerEquipAmulet'
---
# `event` onPlayerEquipAmulet <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This event is triggered when player equips or unequips amulet. When item is unequiped, `null` is returned instead.

## Parameters
```c++
int playerid, string|null instance
```

* `int` **playerid**: the id of the player who equips a amulet.
* `string|null` **instance**: the item instance from Daedalus scripts.

