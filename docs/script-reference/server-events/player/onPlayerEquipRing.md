---
title: 'onPlayerEquipRing'
---
# `event` onPlayerEquipRing <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This event is triggered when player equips or unequips ring. When item is unequiped, `null` item id is returned instead.

## Parameters
```c++
int playerid, int handId, string|null instance
```

* `int` **playerid**: the id of the player who equips a ring.
* `int` **handId**: the hand id that the player is putting the ring on.
* `string|null` **instance**: the item instance from Daedalus scripts.

