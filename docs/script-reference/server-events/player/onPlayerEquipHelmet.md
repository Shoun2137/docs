---
title: 'onPlayerEquipHelmet'
---
# `event` onPlayerEquipHelmet <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player equips or unequips helmet. When item is unequiped, `null` is returned instead.

## Parameters
```c++
int playerid, string|null instance
```

* `int` **playerid**: the id of the player who equips a helmet.
* `string|null` **instance**: the item instance from Daedalus scripts.

