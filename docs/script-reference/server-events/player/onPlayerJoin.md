---
title: 'onPlayerJoin'
---
# `event` onPlayerJoin <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when a player successfully joined the server.

## Parameters
```c++
int playerid
```

* `int` **playerid**: the id of the player who joined the server.

