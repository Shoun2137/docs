---
title: 'onPlayerEnterWorld'
---
# `event` onPlayerEnterWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player entered the world (**ZEN**) and was successfully spawned in it.

## Parameters
```c++
int playerid, string world
```

* `int` **playerid**: the id of the player who entered the world.
* `string` **world**: an filename name of the world.

