---
title: 'onPacket'
---
# `event` onPacket <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.3"

This event is triggered when new packet arrives from the client.

## Parameters
```c++
int playerid, Packet packet
```

* `int` **playerid**: the id of the player who sent the packet.
* `Packet` **packet**: data sended over network from client side.

