---
title: 'isLogicalKeyDisabled'
---
# `function` isLogicalKeyDisabled <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

The function is used to check whether the specified game key action is disabled.

## Declaration
```cpp
bool isLogicalKeyDisabled(int logicalKeyId)
```

## Parameters
* `int` **logicalKeyId**: the id of the logical key. For more information see [Logical key constants](../../../client-constants/logical-key/).
  
## Returns `bool`
`true` when the game key action is disabled, otherwise `false`.

