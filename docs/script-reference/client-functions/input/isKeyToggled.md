---
title: 'isKeyToggled'
---
# `function` isKeyToggled <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

The function is used to check whether the specified keyboard key was toggled from unpressed to pressed state.

## Declaration
```cpp
bool isKeyToggled(int keyId)
```

## Parameters
* `int` **keyId**: the id of the key. For more information see [Key constants](../../../client-constants/key/).
  
## Returns `bool`
`true` when the key was toggled, otherwise `false`.

