---
title: 'disableControls'
---
# `function` disableControls <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    You can use this function, for example: to prevent game from showing menu on ESCAPE.

This function will disable/enable default game actions that are bound to keys.

## Declaration
```cpp
void disableControls(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to disable game keys, otherwise `false`.
  

