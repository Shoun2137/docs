---
title: 'setLogicalKeyBinding'
---
# `function` setLogicalKeyBinding <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function sets keyboard keys binding for game action key.

## Declaration
```cpp
void setLogicalKeyBinding(int logicalKeyId, array|null array)
```

## Parameters
* `int` **logicalKeyId**: the id of the logical key. For more information see [Logical key constants](../../../client-constants/logical-key/).
* `array|null` **array**: containing all of the keyboard keys ids that will bound to logical key, or `null` if you want to unbind all keyboard keys from logical key.
  

