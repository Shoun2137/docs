---
title: 'disableLogicalKey'
---
# `function` disableLogicalKey <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will disable/enable specified game key action, like: Inventory, Draw weapon, Statistics menu, etc.

## Declaration
```cpp
void disableLogicalKey(int logicalKeyId, bool toggle)
```

## Parameters
* `int` **logicalKeyId**: the id of the logical key. For more information see [Logical key constants](../../../client-constants/logical-key/).
* `bool` **toggle**: `true` when you want to disable specified game key action, otherwise `false`.
  

