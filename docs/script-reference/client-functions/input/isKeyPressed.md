---
title: 'isKeyPressed'
---
# `function` isKeyPressed <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

The function is used to check whether the specified keyboard key is pressed.

## Declaration
```cpp
bool isKeyPressed(int keyId)
```

## Parameters
* `int` **keyId**: the id of the key. For more information see [Key constants](../../../client-constants/key/).
  
## Returns `bool`
`true` when the key is pressed, otherwise `false`.

