---
title: 'setGothic1Controls'
---
# `function` setGothic1Controls <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function toggles gothic 1 controls.

## Declaration
```cpp
void setGothic1Controls(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to enable gothic 1 controls, otherwise `false`.
  

