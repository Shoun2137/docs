---
title: 'getKeyboardLocaleName'
---
# `function` getKeyboardLocaleName <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function gets the currently used keyboard locale name, e.g: `"en-US"`.

## Declaration
```cpp
string getKeyboardLocaleName()
```

## Parameters
No parameters.
  
## Returns `string`
Locale name that is currently used by the keyboard. See [Win32 keyboard locales](https://learn.microsoft.com/en-us/windows/win32/msi/localizing-the-error-and-actiontext-tables) for more details.

