---
title: 'getKeyboardCodePage'
---
# `function` getKeyboardCodePage <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function gets the currently used keyboard code page.

## Declaration
```cpp
string getKeyboardCodePage()
```

## Parameters
No parameters.
  
## Returns `string`
Code page that is currently used by the keyboard. See [Win32 code page identifiers](https://learn.microsoft.com/en-us/windows/win32/intl/code-page-identifiers) for more details.

