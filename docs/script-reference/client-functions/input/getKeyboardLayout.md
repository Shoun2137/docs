---
title: 'getKeyboardLayout'
---
# `function` getKeyboardLayout <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function gets the currently used keyboard layout.

## Declaration
```cpp
string getKeyboardLayout()
```

## Parameters
No parameters.
  
## Returns `string`
Layout identifier that is currently used by the keyboard. See [Win32 keyboard layouts](https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/windows-language-pack-default-values) for more details.

