---
title: 'getLogicalKeyBinding'
---
# `function` getLogicalKeyBinding <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function gets all of the keyboard keys bound to game action key.

## Declaration
```cpp
array getLogicalKeyBinding(int logicalKeyId)
```

## Parameters
* `int` **logicalKeyId**: the id of the logical key. For more information see [Logical key constants](../../../client-constants/logical-key/).
  
## Returns `array`
`array` containing all of the keyboard keys ids bound to logical key.

