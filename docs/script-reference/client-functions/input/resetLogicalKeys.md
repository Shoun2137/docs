---
title: 'resetLogicalKeys'
---
# `function` resetLogicalKeys <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function resets all of the logical key bindings (e.g: character control, inventory key, etc.) back to default setttings from one of the presets.

## Declaration
```cpp
void resetLogicalKeys(int mode)
```

## Parameters
* `int` **mode**: mode that represents one of the two possible default controls setttings:<br>`0`: default controls,<br>`1`: alternative controls.
  

