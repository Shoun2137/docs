---
title: 'enable_DamageAnims'
---
# `function` enable_DamageAnims <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.6"
!!! note
    By default damage anims are enabled.

This function will enable/disable damage animations on players.

## Declaration
```cpp
void enable_DamageAnims(bool toggle)
```

## Parameters
* `bool` **toggle**: `false` when you want to disable the animations, otherwise `true`.
  

