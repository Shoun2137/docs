---
title: 'setSightFactor'
---
# `function` setSightFactor <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will set the sight range of the player.

## Declaration
```cpp
void setSightFactor(float factor)
```

## Parameters
* `float` **factor**: the player sight factor (in the range between 0.02-5.0).
  

