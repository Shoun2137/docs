---
title: 'enableEvent_Render'
---
# `function` enableEvent_Render <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"
!!! note
    By default this event is disabled.

This function will enable/disable execution of [onRender](../../../client-events/general/onRender/) event.

## Declaration
```cpp
void enableEvent_Render(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to enable triggering of this event, otherwise `false`.
  

