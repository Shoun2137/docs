---
title: 'setLODStrengthOverride'
---
# `function` setLODStrengthOverride <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will set the LOD strength override used by dynamic models (ASC) like armors, npc body, etc...

## Declaration
```cpp
void setLODStrengthOverride(float strength)
```

## Parameters
* `float` **strength**: the LOD strength value. - `-1` use values stored within mesh. - `0` don't use LOD for mesh. - `1` >= use aggressive LOD
  

