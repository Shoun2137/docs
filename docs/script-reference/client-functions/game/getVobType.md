---
title: 'getVobType'
---
# `function` getVobType <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get the vob type from it's pointer.

## Declaration
```cpp
int getVobType(userpointer ptr)
```

## Parameters
* `userpointer` **ptr**: the pointer to the object from the game.
  
## Returns `int`
The vob type (virtual table), for more information see [Vob constants](../../../client-constants/vob/).

