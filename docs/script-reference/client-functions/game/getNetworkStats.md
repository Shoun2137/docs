---
title: 'getNetworkStats'
---
# `function` getNetworkStats <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to retrieve current network statistics.

## Declaration
```cpp
{packetReceived, packetlossTotal, packetlossLastSecond, messagesInResendBuffer, messageInSendBuffer, bytesInResendBuffer, bytesInSendBuffer} getNetworkStats()
```

## Parameters
No parameters.
  
## Returns `{packetReceived, packetlossTotal, packetlossLastSecond, messagesInResendBuffer, messageInSendBuffer, bytesInResendBuffer, bytesInSendBuffer}`
Statistics of the network connection.

