---
title: 'getTime'
---
# `function` getTime <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

The function is used to get the current time in the game.

## Declaration
```cpp
{day, hour, min} getTime()
```

## Parameters
No parameters.
  
## Returns `{day, hour, min}`
The current time in game.

