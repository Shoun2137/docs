---
title: 'getFpsRate'
---
# `function` getFpsRate <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the client fps rate

## Declaration
```cpp
int getFpsRate()
```

## Parameters
No parameters.
  
## Returns `int`
The current fps rate.

