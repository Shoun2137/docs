---
title: 'getPlayerSpellNr'
---
# `function` getPlayerSpellNr <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function will get spell slot number by given item.

## Declaration
```cpp
int getPlayerSpellNr(int id, string instance)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
  
## Returns `int`
the slot number associated with given slot number with given item. If item is not found, then -1 is returned.

