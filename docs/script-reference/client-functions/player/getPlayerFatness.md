---
title: 'getPlayerFatness'
---
# `function` getPlayerFatness <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc fatness factor.

## Declaration
```cpp
float getPlayerFatness(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `float`
the fatness ratio.

