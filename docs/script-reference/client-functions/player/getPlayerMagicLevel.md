---
title: 'getPlayerMagicLevel'
---
# `function` getPlayerMagicLevel <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.5.2"

This function will get the player/npc magic level.

## Declaration
```cpp
int getPlayerMagicLevel(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the magicLevel.

