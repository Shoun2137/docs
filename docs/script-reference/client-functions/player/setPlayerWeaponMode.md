---
title: 'setPlayerWeaponMode'
---
# `function` setPlayerWeaponMode <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the hero/npc weapon mode.

## Declaration
```cpp
void setPlayerWeaponMode(int id, int weaponMode)
```

## Parameters
* `int` **id**: the player id.
* `int` **weaponMode**: . For more information see [Weapon mode constants](../../../shared-constants/weapon-mode/).
  

