---
title: 'useClosestMob'
---
# `function` useClosestMob <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"

This function will try to trigger interaction between hero/npc and nearest Mob object.

## Declaration
```cpp
void useClosestMob(int id, string scemeName, int targetState)
```

## Parameters
* `int` **id**: the player id.
* `string` **scemeName**: the animation sceme name, e.g: `"BENCH"` when you want to interact with bench.
* `int` **targetState**: the target state, use `1` if you want to start interaction with the Mob or `-1` if you want to end it.
  

