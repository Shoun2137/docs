---
title: 'setPlayerVisualAlpha'
---
# `function` setPlayerVisualAlpha <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will set player visual alpha.  
Visual alpha is the transparency effect which was used on Quarchodron in singleplayer game.

## Declaration
```cpp
bool setPlayerVisualAlpha(int id, float alpha)
```

## Parameters
* `int` **id**: the player id.
* `float` **alpha**: the alpha value in range <0.0, 1.0>.
  
## Returns `bool`
`true` if visual effect was successfully set, otherwise `false`.

