---
title: 'addEffect'
---
# `function` addEffect <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4.9"

This function is used to add visual effect on player.  
Player can have more than one active visual effect on itself.

## Declaration
```cpp
bool addEffect(int id, string effect)
```

## Parameters
* `int` **id**: the player id.
* `string` **effect**: the name of the VisualFX, e.g: `"SPELLFX_MAYA_GHOST"`.
  
## Returns `bool`
`true` if effect was successfully added, otherwise `false`.

