---
title: 'getPlayerMaxMana'
---
# `function` getPlayerMaxMana <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"

This function will get the player/npc max mana points.

## Declaration
```cpp
int getPlayerMaxMana(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the maximum mana points amount.

