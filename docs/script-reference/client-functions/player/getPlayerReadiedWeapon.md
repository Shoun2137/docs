---
title: 'getPlayerReadiedWeapon'
---
# `function` getPlayerReadiedWeapon <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get player readied (drawn) weapon.

## Declaration
```cpp
string getPlayerReadiedWeapon(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the item instance from Daedalus scripts.

