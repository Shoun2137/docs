---
title: 'unreadySpell'
---
# `function` unreadySpell <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will cause hero/npc to unready active spell.  
It works almost the same as [removeWeapon](../removeWeapon), but also stops hero if he's moving before hiding the active spell.

## Declaration
```cpp
void unreadySpell(int id)
```

## Parameters
* `int` **id**: the player id.
  

