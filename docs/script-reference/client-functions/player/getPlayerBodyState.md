---
title: 'getPlayerBodyState'
---
# `function` getPlayerBodyState <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1.9"

This function will return player bodystate.

## Declaration
```cpp
int getPlayerBodyState(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the player bodystate. For more information see [BodyStates](../../../client-constants/bodystate/).

