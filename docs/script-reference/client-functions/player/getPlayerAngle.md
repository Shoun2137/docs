---
title: 'getPlayerAngle'
---
# `function` getPlayerAngle <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc facing rotation on y axis.

## Declaration
```cpp
float getPlayerAngle(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `float`
the facing rotation on y axis.

