---
title: 'playAni'
---
# `function` playAni <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function is used to play animation on player/npc.

## Declaration
```cpp
void playAni(int id, string aniName)
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: the name of the animation, e.g: `"T_STAND_2_SIT"`.
  

