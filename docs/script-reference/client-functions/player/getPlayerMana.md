---
title: 'getPlayerMana'
---
# `function` getPlayerMana <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"

This function will get the player/npc mana points.

## Declaration
```cpp
int getPlayerMana(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the mana points amount.

