---
title: 'removePlayerOverlay'
---
# `function` removePlayerOverlay <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will remove animation overlay from player/npc.

## Declaration
```cpp
bool removePlayerOverlay(int id, int overlayId)
```

## Parameters
* `int` **id**: the player id.
* `int` **overlayId**: . the overlay id from `mds.xml` file, e.g: `Mds.id("HUMANS_MILITIA.MDS")`
  
## Returns `bool`
`true` if animation overlay was successfully removed from npc, otherwise `false`.

