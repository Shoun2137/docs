---
title: 'getPlayerType'
---
# `function` getPlayerType <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"
!!! note
    Return value can also be `-1` if player isn't connected or when npc isn't created.

This function will get the player/npc type.

The possible return values are:  
[OBJ_PLAYER](../../../client-constants/objecttype), [OBJ_LOCALPLAYER](../../../client-constants/objecttype), [OBJ_NPC](../../../client-constants/objecttype), [OBJ_LOCALNPC](../../../client-constants/objecttype).

## Declaration
```cpp
int getPlayerType(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the player type.

