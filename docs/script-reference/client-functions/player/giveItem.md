---
title: 'giveItem'
---
# `function` giveItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function is used to give item for player/npc.

## Declaration
```cpp
void giveItem(int id, string instance, int amount)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
* `int` **amount**: the amount of item, e.g: `1000` gold coins.
  

