---
title: 'getLastHitAniFrame'
---
# `function` getLastHitAniFrame <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4.9"

This function will get the last hit ani frame from player.

## Declaration
```cpp
int getLastHitAniFrame(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the last hit ani frame id.

