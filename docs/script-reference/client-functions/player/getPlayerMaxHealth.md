---
title: 'getPlayerMaxHealth'
---
# `function` getPlayerMaxHealth <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc max health points.

## Declaration
```cpp
int getPlayerMaxHealth(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the maximum health points amount.

