---
title: 'getPlayerStrength'
---
# `function` getPlayerStrength <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the player/npc strength points.

## Declaration
```cpp
int getPlayerStrength(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the strength points amount.

