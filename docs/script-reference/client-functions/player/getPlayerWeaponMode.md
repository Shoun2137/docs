---
title: 'getPlayerWeaponMode'
---
# `function` getPlayerWeaponMode <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc weapon mode.

## Declaration
```cpp
int getPlayerWeaponMode(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
weaponMode, for more information see [Weapon mode constants](../../../shared-constants/weapon-mode/).

