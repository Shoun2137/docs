---
title: 'unequipItem'
---
# `function` unequipItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    If you want to unequip weapon/shield, first make sure that player is in `WEAPONMODE_NONE`.

This function is used to unequip item from player/npc.

## Declaration
```cpp
bool unequipItem(int id, string instance)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
  
## Returns `bool`
`true` if item was successfully unequipped, otherwise `false`.

