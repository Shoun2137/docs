---
title: 'getPlayerName'
---
# `function` getPlayerName <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc nickname set for client.

## Declaration
```cpp
string getPlayerName(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player nickname.

