---
title: 'getPlayerVisualAlpha'
---
# `function` getPlayerVisualAlpha <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get player visual alpha.

## Declaration
```cpp
float getPlayerVisualAlpha(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `float`
the player visual alpha.

