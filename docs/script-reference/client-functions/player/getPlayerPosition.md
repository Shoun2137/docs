---
title: 'getPlayerPosition'
---
# `function` getPlayerPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc world position.

## Declaration
```cpp
Vec3 getPlayerPosition(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Vec3`
the player world position.

