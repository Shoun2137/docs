---
title: 'getPlayerSpellItem'
---
# `function` getPlayerSpellItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"
!!! note
    Equipped spells are numbered from 0 to 6.

This function will get equipped spell item on given slot number.

## Declaration
```cpp
int getPlayerSpellItem(int id, int nr)
```

## Parameters
* `int` **id**: the player id.
* `int` **nr**: the slot number.
  
## Returns `int`
the id of item associated with given slot number. If item not is registered on this slot, then -1 is returned.

