---
title: 'setPlayerCollision'
---
# `function` setPlayerCollision <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function is used to toggle player/npc dynamic collision detection.

## Declaration
```cpp
void setPlayerCollision(int id, bool toggle)
```

## Parameters
* `int` **id**: the player id.
* `bool` **toggle**: `true` when you want to enable dynamic collision detection for player, otherwise `false`.
  

