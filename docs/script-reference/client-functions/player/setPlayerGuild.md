---
title: 'setPlayerGuild'
---
# `function` setPlayerGuild <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function will set the player/npc guild.

## Declaration
```cpp
void setPlayerGuild(int id, int guildId)
```

## Parameters
* `int` **id**: the player id.
* `int` **guildId**: the guild id.
  

