---
title: 'getPlayerSpellCount'
---
# `function` getPlayerSpellCount <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function will get equipped spell items count.

## Declaration
```cpp
int getPlayerSpellCount(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
equipped spell items count.

