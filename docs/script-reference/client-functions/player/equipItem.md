---
title: 'equipItem'
---
# `function` equipItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    If you want to equip weapon/shield, first make sure that player is in `WEAPONMODE_NONE`.

This function is used to equip item on player/npc.

## Declaration
```cpp
bool equipItem(int id, string instance, int slotId = -1)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
* `int` **slotId**: the slot id in which you want to equip item on player, e.g scrolls, runes, by default the item will be equipped on the first free slot.
  
## Returns `bool`
`true` if item was successfully equipped, otherwise `false`.

