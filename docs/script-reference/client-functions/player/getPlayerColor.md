---
title: 'getPlayerColor'
---
# `function` getPlayerColor <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc nickname color for client.

## Declaration
```cpp
Color getPlayerColor(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Color`
the player nickname color.

