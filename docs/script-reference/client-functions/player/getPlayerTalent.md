---
title: 'getPlayerTalent'
---
# `function` getPlayerTalent <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get the player talent.

## Declaration
```cpp
void getPlayerTalent(int id, int talentId, bool toggle)
```

## Parameters
* `int` **id**: the player id.
* `int` **talentId**: the talent id. For more information see [Talent constants](../../../shared-constants/talent/).
* `bool` **toggle**: `true` if talent is enabled for player, otherwise `false`.
  

