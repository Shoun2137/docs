---
title: 'setPlayerOnFloor'
---
# `function` setPlayerOnFloor <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will try to put player/npc on floor.  
If the difference between player y position and the floor y position is less or equal 1000, the function will succeed.

## Declaration
```cpp
void setPlayerOnFloor(int id)
```

## Parameters
* `int` **id**: the player id.
  

