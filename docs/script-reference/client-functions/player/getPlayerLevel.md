---
title: 'getPlayerLevel'
---
# `function` getPlayerLevel <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.5.2"

This function will get the player/npc level.

## Declaration
```cpp
int getPlayerLevel(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the level.

