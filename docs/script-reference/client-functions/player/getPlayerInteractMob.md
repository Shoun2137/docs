---
title: 'getPlayerInteractMob'
---
# `function` getPlayerInteractMob <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to get current interacted mob by player.

## Declaration
```cpp
userpointer getPlayerInteractMob(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `userpointer`
the current interacted mob.

