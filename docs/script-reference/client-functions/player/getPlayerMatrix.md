---
title: 'getPlayerMatrix'
---
# `function` getPlayerMatrix <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get player matrix reference.

## Declaration
```cpp
Mat4 getPlayerMatrix(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Mat4`
the player matrix reference.

