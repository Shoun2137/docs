---
title: 'setPlayerMagicLevel'
---
# `function` setPlayerMagicLevel <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.5.2"

This function will set the player/npc magic level.

## Declaration
```cpp
void setPlayerMagicLevel(int id, int magicLevel)
```

## Parameters
* `int` **id**: the player id.
* `int` **magicLevel**: the magic level.
  

