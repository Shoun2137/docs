---
title: 'setPlayerMana'
---
# `function` setPlayerMana <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"

This function will set the player/npc mana points.

## Declaration
```cpp
void setPlayerMana(int id, int mana)
```

## Parameters
* `int` **id**: the player id.
* `int` **mana**: the mana points amount.
  

