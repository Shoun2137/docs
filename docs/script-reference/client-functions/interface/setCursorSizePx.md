---
title: 'setCursorSizePx'
---
# `function` setCursorSizePx <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will set the mouse cursor size on screen in pixels.

## Declaration
```cpp
void setCursorSizePx(int width, int height)
```

## Parameters
* `int` **width**: the width of the cursor in pixels.
* `int` **height**: the height of the cursor in pixels.
  

