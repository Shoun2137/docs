---
title: 'setBarPosition'
---
# `function` setBarPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

This function will set the specified status bar position on the screen.

## Declaration
```cpp
void setBarPosition(int type, int x, int y)
```

## Parameters
* `int` **type**: the type of status bar you want to set the position of. For more information see [HUD constants](../../../client-constants/hud/).
* `int` **x**: position on the screen in virtuals.
* `int` **y**: position on the screen in virtuals.
  

