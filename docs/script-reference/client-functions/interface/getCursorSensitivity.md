---
title: 'getCursorSensitivity'
---
# `function` getCursorSensitivity <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the mouse cursor sensitivity.

## Declaration
```cpp
float getCursorSensitivity()
```

## Parameters
No parameters.
  
## Returns `float`
sensitivity the value in range (0.0, 10.0).

