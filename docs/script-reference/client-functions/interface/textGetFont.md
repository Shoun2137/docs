---
title: 'textGetFont'
---
# `function` textGetFont <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the screen font.

## Declaration
```cpp
string textGetFont()
```

## Parameters
No parameters.
  
## Returns `string`
the current screen font.

