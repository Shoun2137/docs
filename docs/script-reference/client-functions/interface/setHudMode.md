---
title: 'setHudMode'
---
# `function` setHudMode <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

This function will change the visibilty of HUD elements.

## Declaration
```cpp
void setHudMode(int type, int mode)
```

## Parameters
* `int` **type**: the type of HUD element you want to change visibility of. For more information see [HUD constants](../../../client-constants/hud/).
* `int` **mode**: the mode type. For more information see [HUD constants](../../../client-constants/hud/).
  

