---
title: 'setCursorPosition'
---
# `function` setCursorPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the mouse cursor position on screen in virtuals.

## Declaration
```cpp
void setCursorPosition(int x, int y)
```

## Parameters
* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
  

