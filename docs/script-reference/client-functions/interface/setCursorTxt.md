---
title: 'setCursorTxt'
---
# `function` setCursorTxt <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This function will set the mouse cursor texture.

## Declaration
```cpp
void setCursorTxt(string txt)
```

## Parameters
* `string` **txt**: the cursor texture.
  

