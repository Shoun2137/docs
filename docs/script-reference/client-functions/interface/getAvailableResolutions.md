---
title: 'getAvailableResolutions'
---
# `function` getAvailableResolutions <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the array of available resolutions supported by client's computer.

## Declaration
```cpp
[{x, y, bpp}...] getAvailableResolutions()
```

## Parameters
No parameters.
  
## Returns `[{x, y, bpp}...]`
Array of supported resolutions by the hardware.

