---
title: 'setCursorPositionPx'
---
# `function` setCursorPositionPx <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This function will set the mouse cursor position on screen in pixels.

## Declaration
```cpp
void setCursorPositionPx(int x, int y)
```

## Parameters
* `int` **x**: the x position in pixels.
* `int` **y**: the y position in pixels.
  

