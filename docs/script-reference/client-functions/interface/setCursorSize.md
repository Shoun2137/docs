---
title: 'setCursorSize'
---
# `function` setCursorSize <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will set the mouse cursor size on screen in virtuals.

## Declaration
```cpp
void setCursorSize(int width, int height)
```

## Parameters
* `int` **width**: the width of the cursor in virtuals.
* `int` **height**: the height of the cursor in virtuals.
  

