---
title: 'nax'
---
# `function` nax <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    Use this function only when you want to convert x position or width on the screen.

This function will convert virtuals to pixels on screen X dimension and return it as a result.

## Declaration
```cpp
int nax(int virtuals)
```

## Parameters
* `int` **virtuals**: the virtuals to convert.
  
## Returns `int`
the pixels after conversion.

