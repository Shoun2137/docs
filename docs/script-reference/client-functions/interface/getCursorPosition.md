---
title: 'getCursorPosition'
---
# `function` getCursorPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the mouse cursor position on screen in virtuals.

## Declaration
```cpp
Vec2i getCursorPosition()
```

## Parameters
No parameters.
  
## Returns `Vec2i`
the cursor position on screen.

