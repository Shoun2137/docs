---
title: 'isCursorVisible'
---
# `function` isCursorVisible <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function is used to check whether is mouse cursor visible on the screen.

## Declaration
```cpp
bool isCursorVisible()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` if the mouse cursor is visible on the screen, otherwise `false`.

