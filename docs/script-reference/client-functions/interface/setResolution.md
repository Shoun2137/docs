---
title: 'setResolution'
---
# `function` setResolution <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will set the current resolution of the game window.

## Declaration
```cpp
bool setResolution(int x, int y, int bpp)
```

## Parameters
* `int` **x**: the width of the window.
* `int` **y**: the height of the window.
* `int` **bpp**: the bytes per pixel.
  
## Returns `bool`
`true` when the resolution is supported and has been set, otherwise `false`.

