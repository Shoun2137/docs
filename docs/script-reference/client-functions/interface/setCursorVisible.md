---
title: 'setCursorVisible'
---
# `function` setCursorVisible <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will toggle the visibility of the mouse cursor on the screen.

## Declaration
```cpp
void setCursorVisible(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` if the mouse cursor should be visible on the screen, otherwise `false`.
  

