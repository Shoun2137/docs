---
title: 'getCursorPositionPx'
---
# `function` getCursorPositionPx <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This function will get the mouse cursor position on screen in pixels.

## Declaration
```cpp
Vec2i getCursorPositionPx()
```

## Parameters
No parameters.
  
## Returns `Vec2i`
the cursor position on screen.

