---
title: 'setCursorSensitivity'
---
# `function` setCursorSensitivity <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the mouse cursor sensitivity.

## Declaration
```cpp
void setCursorSensitivity(float sensitivity)
```

## Parameters
* `float` **sensitivity**: the value in range (0.0, 10.0).
  

