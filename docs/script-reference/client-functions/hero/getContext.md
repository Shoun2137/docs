---
title: 'getContext'
---
# `function` getContext <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to get script context.
For more information see [this article](../../../../multiplayer/script-context/).

## Declaration
```cpp
int getContext(int type)
```

## Parameters
* `int` **type**: the type of context.
  
## Returns `int`
value the value stored within selected context.

