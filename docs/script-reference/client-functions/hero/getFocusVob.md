---
title: 'getFocusVob'
---
# `function` getFocusVob <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to get current focused vob by hero.

## Declaration
```cpp
userpointer getFocusVob()
```

## Parameters
No parameters.
  
## Returns `userpointer`
the current focused vob.

