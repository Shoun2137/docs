---
title: 'setContext'
---
# `function` setContext <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to set script context.
For more information see [this article](../../../../multiplayer/script-context/).

## Declaration
```cpp
void setContext(int type, int value)
```

## Parameters
* `int` **type**: the type of modified context.
* `int` **value**: the new value written into context.
  

