---
title: 'setFreeze'
---
# `function` setFreeze <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will blocks/unblocks the ability to control hero.

## Declaration
```cpp
void setFreeze(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to freeze the hero, otherwise `false`.
  

