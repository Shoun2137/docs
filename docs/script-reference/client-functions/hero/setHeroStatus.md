---
title: 'setHeroStatus'
---
# `function` setHeroStatus <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will set the hero status that is responsible for selecting active music theme (_STD, _THR, _FGT).

## Declaration
```cpp
void setHeroStatus(int status)
```

## Parameters
* `int` **status**: the hero status. For more information see [Hero Status constants](../../../client-constants/hero-status/).
  

