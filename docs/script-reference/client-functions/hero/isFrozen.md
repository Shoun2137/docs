---
title: 'isFrozen'
---
# `function` isFrozen <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether client can't control hero.

## Declaration
```cpp
bool isFrozen()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` when client cannot control hero, otherwise `false`.

