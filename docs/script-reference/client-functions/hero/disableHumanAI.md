---
title: 'disableHumanAI'
---
# `function` disableHumanAI <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will enable or disable human AI that is responsible for allowing player to controll the character.

## Declaration
```cpp
void disableHumanAI(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to disable human AI for the hero, otherwise `true`.
  

