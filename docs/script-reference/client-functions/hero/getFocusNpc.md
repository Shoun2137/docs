---
title: 'getFocusNpc'
---
# `function` getFocusNpc <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function is used to get current focused npc by hero.

## Declaration
```cpp
int getFocusNpc()
```

## Parameters
No parameters.
  
## Returns `int`
the current focused npc. In case were there is no focus returns `-1`.

