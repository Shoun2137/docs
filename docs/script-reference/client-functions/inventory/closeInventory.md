---
title: 'closeInventory'
---
# `function` closeInventory <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will hide the game inventory UI.

## Declaration
```cpp
void closeInventory()
```

## Parameters
No parameters.
  

