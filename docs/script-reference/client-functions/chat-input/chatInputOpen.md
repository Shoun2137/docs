---
title: 'chatInputOpen'
---
# `function` chatInputOpen <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will show the text input on the screen.  
Client can type the text only when chat input is opened.

## Declaration
```cpp
void chatInputOpen()
```

## Parameters
No parameters.
  

