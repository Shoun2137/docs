---
title: 'chatInputSetPosition'
---
# `function` chatInputSetPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the text input position on the screen.

## Declaration
```cpp
void chatInputSetPosition(int x, int y)
```

## Parameters
* `int` **x**: position on the screen in virtuals.
* `int` **y**: position on the screen in virtuals.
  

