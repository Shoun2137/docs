---
title: 'chatInputClear'
---
# `function` chatInputClear <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will clear chat input text.

## Declaration
```cpp
void chatInputClear()
```

## Parameters
No parameters.
  

