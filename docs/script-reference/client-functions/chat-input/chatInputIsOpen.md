---
title: 'chatInputIsOpen'
---
# `function` chatInputIsOpen <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether chat input is opened.

## Declaration
```cpp
bool chatInputIsOpen()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` when chat input is visible, otherwise `false`.

