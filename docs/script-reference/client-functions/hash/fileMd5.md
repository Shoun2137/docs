---
title: 'fileMd5'
---
# `function` fileMd5 <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will try to calculate the MD5 hash of the file content it as hexadecimal text representation.

## Declaration
```cpp
string fileMd5(string path)
```

## Parameters
* `string` **path**: the path to a file.
  
## Returns `string`
Returns string hash as hexadecimal text representation.

