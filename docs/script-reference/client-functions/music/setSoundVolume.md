---
title: 'setSoundVolume'
---
# `function` setSoundVolume <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"

This function will set the ingame sound volume.

## Declaration
```cpp
void setSoundVolume(float volume)
```

## Parameters
* `float` **volume**: the value in range <0.0, 1.0>.
  

