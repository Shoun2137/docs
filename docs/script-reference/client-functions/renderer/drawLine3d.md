---
title: 'drawLine3d'
---
# `function` drawLine3d <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will draw 3d line on the screen using world coordinates.

## Declaration
```cpp
void drawLine3d(float int, float int, float int, float int, float int, float int, int r, int g, int b)
```

## Parameters
* `float` **int**: x1 the position on X axis of the begin point.
* `float` **int**: y1 the position on Y axis of the begin point.
* `float` **int**: z1 the position on Z axis of the begin point.
* `float` **int**: x2 the position on X axis of the end point.
* `float` **int**: y2 the position on Y axis of the end point.
* `float` **int**: z2 the position on Z axis of the end point.
* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

