---
title: 'Network'
---
# `constants` Network <font size="4">(server-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `DISCONNECTED` | Represents disconnected player state. |
| `LOST_CONNECTION` | Represents lost connection player state. |
| `HAS_CRASHED` | Represents crash player state. |
