---
title: 'AntiCheat'
---
# `constants` AntiCheat <font size="4">(server-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `AC_SPEED_HACK` | Represents speed hack state. |
