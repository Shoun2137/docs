---
title: 'queue'
---
# `class` queue <font size="4">(shared-side)</font>
!!! info "Available since version: 0.1.0"

This class represents FIFO queue.


## Properties
No properties.

----

## Methods
### len

This method will get size of the queue.

```cpp
int len()
```

  
**Returns `int`:**

the size of the queue.

----
### front

This method will get the oldest element from the queue.

```cpp
any front()
```

  
**Returns `any`:**

the element that is placed on front of the queue.

----
### back

This method will get the newest element from the queue.

```cpp
any back()
```

  
**Returns `any`:**

the element that is placed on back of the queue.

----
### push

This method will insert element at the begining of the queue.
The added element, can be acces by [queue.back](../queue/#front) method.

```cpp
void push()
```

  

----
### pop

This method will pop oldest element from the queue and returns it.

```cpp
any pop()
```

  
**Returns `any`:**

the element that was at the front of the queue.

----
### clear
!!! info "Available since version: 0.1.10"

This method will remove all elements from the queue.

```cpp
void clear()
```

  

----
