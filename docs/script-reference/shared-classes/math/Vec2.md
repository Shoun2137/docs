---
title: 'Vec2'
---
# `class` Vec2 <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents 2d vector.

### Constructor
```cpp
Vec2()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Vec2(float value)
```

**Parameters:**

* `float` **value**: the initial value for x, y components.
### Constructor
```cpp
Vec2(float x, float y)
```

**Parameters:**

* `float` **x**: the initial value for x component.
* `float` **y**: the initial value for y component.

## Properties
### `float` x 

Represents the x component of the vector.

----
### `float` y 

Represents the y component of the vector.

----

## Methods
### len

This method will return the vector length.  
The length of the vector is square root of `(x*x + y*y)`.

```cpp
float len()
```

  
**Returns `float`:**

the squared length of the vector.

----
### len2

This method will get the squared vector length.  
The length of the vector is `(x*x + y*y)`.  
Calculating the squared length instead of the [len](#len) is much faster.

```cpp
float len2()
```

  
**Returns `float`:**

the length of the vector.

----
### lenApprox

This method will get the approximate vector length.  
The length of the vector is approximation square root of `(x*x + y*y)`.  
This method is faster than [len](#len) but less accurate.

```cpp
float lenApprox()
```

  
**Returns `float`:**

the length of the vector.

----
### distance

This method will get distance between two vectors.

```cpp
float distance(Vec2 vec)
```

**Parameters:**

* `Vec2` **vec**: the vector that will be used in distance calculation.
  
**Returns `float`:**

the distance between vectors.

----
### normalize
!!! note
    Avoid normalizing a vector with length = `0`.

This method will normalize the vector.  
Normalizing a vector will cause it's length to be equal to `1`.  
The direction of the vector won't be affected.

```cpp
Vec2& normalize()
```

  
**Returns `Vec2&`:**



----
### normalizeSafe

This method will normalize the vector.
It works almost exactly the same as [normalize](#normalize)
with the difference that zero length vector won't cause any issues.

```cpp
Vec2& normalizeSafe()
```

  
**Returns `Vec2&`:**



----
### normalizeApprox
!!! note
    Avoid normalizing a vector with length = `0`.

This method will normalize the vector with small approximation.  
It works almost the same as [normalize](#normalize), but it's faster and less accurate.  
It uses the `fast inverse square root` algorithm as the base of calculating square root.

```cpp
Vec2& normalizeApprox()
```

  
**Returns `Vec2&`:**



----
### set

This method will set all of the vector components.

```cpp
void set(float x, float y)
```

**Parameters:**

* `float` **x**: the new vector x component.
* `float` **y**: the new vector y component.
  

----
### isEqualEps

This method will return `true` if vectors are approximately the same.  
The minimal difference used for comparison is `0.001`.

```cpp
bool isEqualEps(Vec2 vec)
```

**Parameters:**

* `Vec2` **vec**: the vector that will be used in comparision.
  
**Returns `bool`:**

`true` if vectors are approximately the same, otherwise `false`.

----
### abs

This method will return vector with absolute x,y components.

```cpp
Vec2 abs()
```

  
**Returns `Vec2`:**

the new vector result.

----
### `static` swap

This method will swap x,y components between two vectors.

```cpp
void swap(Vec2 vec1, Vec2 vec2)
```

**Parameters:**

* `Vec2` **vec1**: the vector that will be used in swap operation.
* `Vec2` **vec2**: the vector that will be used in swap operation.
  

----
### `static` min

This method will return vector with the smallest x,y components from two vectors.

```cpp
Vec2 min(Vec2 vec1, Vec2 vec2)
```

**Parameters:**

* `Vec2` **vec1**: the vector that will be used in min operation.
* `Vec2` **vec2**: the vector that will be used in min operation.
  
**Returns `Vec2`:**

the new vector result.

----
### `static` max

This method will return vector with the largest x,y components from two vectors.

```cpp
Vec2 max(Vec2 vec1, Vec2 vec2)
```

**Parameters:**

* `Vec2` **vec1**: the vector that will be used in max operation.
* `Vec2` **vec2**: the vector that will be used in max operation.
  
**Returns `Vec2`:**

the new vector result.

----
### `static` prod

This method will multiply two vectors and return the result as a new vector.

```cpp
Vec2 prod(Vec2 vec1, Vec2 vec2)
```

**Parameters:**

* `Vec2` **vec1**: the vector that will be used in prod operation.
* `Vec2` **vec2**: the vector that will be used in prod operation.
  
**Returns `Vec2`:**

the new vector result.

----
### `static` dot

This method will return the dot product between two vectors.  
For normalized vectors dot returns:  
- `1` if they point in exactly the same direction
- `-1` if they point in completely opposite directions
and a number in between for other cases (e.g. Dot returns zero if vectors are perpendicular).  
For vectors of arbitrary length the Dot return values are similar: they get larger when the angle between vectors decreases.

```cpp
float dot(Vec2 vec1, Vec2 vec2)
```

**Parameters:**

* `Vec2` **vec1**: the vector that will be used in dot operation.
* `Vec2` **vec2**: the vector that will be used in dot operation.
  
**Returns `float`:**

the dot product.

----
### `static` lerp

This method will return interpolated vector between vectors a and b by ratio t.

```cpp
Vec2 lerp(float t, Vec2 v1, Vec2 v2)
```

**Parameters:**

* `float` **t**: interpolation ratio.
* `Vec2` **v1**: the vector used in lerp operation.
* `Vec2` **v2**: the vector used in lerp operation.
  
**Returns `Vec2`:**

the lerp vector result.

----
