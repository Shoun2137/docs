---
title: 'Color'
---
# `class` Color <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents rgba color.

### Constructor
```cpp
Color()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Color(int r, int g, int b)
```

**Parameters:**

* `int` **r**: the red color component as value in range <0, 255>.
* `int` **g**: the green color component as value in range <0, 255>.
* `int` **b**: the blue color component as value in range <0, 255>.
### Constructor
```cpp
Color(int r, int g, int b, int a)
```

**Parameters:**

* `int` **r**: the red color component as value in range <0, 255>.
* `int` **g**: the green color component as value in range <0, 255>.
* `int` **b**: the blue color component as value in range <0, 255>.
* `int` **a**: the alpha color component as value in range <0, 255>.
### Constructor
```cpp
Color(int dword)
```

**Parameters:**

* `int` **dword**: the integer representing bgra color.

## Properties
### `int` r 
!!! info "Available since version: 0.3.0"

Represents the red color component as value in range <0, 255>.

----
### `int` g 
!!! info "Available since version: 0.3.0"

Represents the green color component as value in range <0, 255>.

----
### `int` b 
!!! info "Available since version: 0.3.0"

Represents the blue color component as value in range <0, 255>.

----
### `int` a 
!!! info "Available since version: 0.3.0"

Represents the alpha color component as value in range <0, 255>.

----
### `int` dword 
!!! info "Available since version: 0.3.0"

Represents the total sum of color as 32 bit integer.

----
### `float` intensity 
!!! info "Available since version: 0.3.0"

Represents the color brightness scale [(Rec. 601)](https://vishald.com/blog/is-a-color-dark/#luma-coefficients).

----

## Methods
### set
!!! info "Available since version: 0.3.0"

This method sets the RGBA color components.

```cpp
void set(int r, int g, int b, int a = 255)
```

**Parameters:**

* `int` **r**: the red color component as value in range <0, 255>.
* `int` **g**: the green color component as value in range <0, 255>.
* `int` **b**: the blue color component as value in range <0, 255>.
* `int` **a**: the alpha color component as value in range <0, 255>.
  

----
