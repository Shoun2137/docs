---
title: 'onWindowFocus'
---
# `event` onWindowFocus <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when main game window gains or loses focus.

## Parameters
```c++
bool focused
```

* `bool` **focused**: `true` when window gets focused, otherwise `false`.

