---
title: 'onPacket'
---
# `event` onPacket <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.3"

This event is triggered when new packet arrives from the server.

## Parameters
```c++
Packet packet
```

* `Packet` **packet**: data sended over network from server side.

