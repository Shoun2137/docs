---
title: 'onRender'
---
# `event` onRender <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    This event is disabled by default. To enable it, use [enableEvent_Render](../../../client-functions/game/enableEvent_Render) function.

This event is triggered every game frame.

## Parameters
No parameters.

