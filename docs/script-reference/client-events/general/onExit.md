---
title: 'onExit'
---
# `event` onExit <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"
!!! tip "This event can be canceled"
!!! note
    This event can be cancelled in order to prevent game from closing.

This event is triggered right before multiplayer client disconnect from server and clear all resources.

## Parameters
No parameters.

