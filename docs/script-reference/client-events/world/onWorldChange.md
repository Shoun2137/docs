---
title: 'onWorldChange'
---
# `event` onWorldChange <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"
!!! tip "This event can be canceled"
!!! note
    This event can be only cancelled, when is triggered by map (**ZEN**) scripts. In any other case, you cannot prevent from world change.

This event is triggered when user tries to change currently played world (**ZEN**).

## Parameters
```c++
string world
```

* `string` **world**: an filename name of the world.

