---
title: 'onWorldEnter'
---
# `event` onWorldEnter <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This event is triggered when changed world (**ZEN**) is loaded and hero is successfully spawned in it.

## Parameters
```c++
string world
```

* `string` **world**: an filename name of the world.

