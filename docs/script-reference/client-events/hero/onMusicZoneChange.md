---
title: 'onMusicZoneChange'
---
# `event` onMusicZoneChange <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    The event will be called even when oldMusicZone is the same as newMusicZone.

This event is triggered when hero is changing the music zone to different one.

## Parameters
```c++
string oldMusicZone, string newMusicZone
```

* `string` **oldMusicZone**: the name of music zone in which the hero was before the change.
* `string` **newMusicZone**: the name of music zone in which the hero is currently located.

