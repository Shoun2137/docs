---
title: 'onFocusCollect'
---
# `event` onFocusCollect <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered when hero collects new nearby objects that can be focused and stores them in array.  
The resulted array will be used by the game to determine which object can be focused by the hero (if any).

## Parameters
```c++
zarray& focuslist
```

* `zarray&` **focuslist**: the array containing nearby objects.

