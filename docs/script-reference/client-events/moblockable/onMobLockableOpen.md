---
title: 'onMobLockableOpen'
---
# `event` onMobLockableOpen <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when hero/npc opens up lockable mobs (doors or chests) in the world.

## Parameters
```c++
int playerid, userdata address, int type
```

* `int` **playerid**: the id of the hero/npc that is opening up lockable mob.
* `userdata` **address**: the address of lockable mob.
* `int` **type**: the type of lockable mob. For more information see [Vob types](../../../client-constants/vob/).

