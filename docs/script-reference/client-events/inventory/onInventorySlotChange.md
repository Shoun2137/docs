---
title: 'onInventorySlotChange'
---
# `event` onInventorySlotChange <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"

This event is triggered when hero changes selected item in opened inventory. Slots are numbered from left to right.

## Parameters
```c++
int from, int to
```

* `int` **from**: represents previous slot number.
* `int` **to**: represents current slot number.

