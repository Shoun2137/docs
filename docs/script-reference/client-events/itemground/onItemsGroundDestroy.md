---
title: 'onItemsGroundDestroy'
---
# `event` onItemsGroundDestroy <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when every item ground gets destroyed while chaning world/virtual world.

## Parameters
No parameters.

