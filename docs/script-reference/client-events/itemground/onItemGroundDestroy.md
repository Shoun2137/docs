---
title: 'onItemGroundDestroy'
---
# `event` onItemGroundDestroy <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    This event won't be called when item grounds gets destroyed while changing world/virtual world.

This event is triggered when server item ground gets picked up by the player.

## Parameters
```c++
ItemGround item
```

* `ItemGround` **item**: object representing item on the ground.

