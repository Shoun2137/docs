---
title: 'onMouseWheel'
---
# `event` onMouseWheel <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This event triggers whenever the user scrolls up or down wheel on his mouse.

## Parameters
```c++
int z
```

* `int` **z**: this value represents direction in which user is scrolling. If value is positive, then user is scrolling up, otherwise it means is scrolling down.

