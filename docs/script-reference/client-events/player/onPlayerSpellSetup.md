---
title: 'onPlayerSpellSetup'
---
# `event` onPlayerSpellSetup <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered when player is opening spell in hand.

## Parameters
```c++
int playerid, Item item
```

* `int` **playerid**: the id of the player whose opened spell.
* `Item` **item**: the item object. For more information see [Item class](../../../client-classes/game/Item/).

