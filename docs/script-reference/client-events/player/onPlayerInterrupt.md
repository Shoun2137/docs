---
title: 'onPlayerInterrupt'
---
# `event` onPlayerInterrupt <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when hero/npc gets interrupted by the game while doing something, e.g: hero/npc gets attacked during interaction with mob inter object.

## Parameters
```c++
int playerid, bool dontStopMobInteraction, bool stopSelectedSpell
```

* `int` **playerid**: the id of the hero/npc that gets interrupted.
* `bool` **dontStopMobInteraction**: `true` when interrupt action doesn't force stopping interaction with mob inter, otherwise `false`.
* `bool` **stopSelectedSpell**: `true` when interrupt action force stopping investing selected spell, otherwise `false`.

