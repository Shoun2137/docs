---
title: 'onPlayerRespawn'
---
# `event` onPlayerRespawn <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when player is respawned after death by the server.

## Parameters
No parameters.

