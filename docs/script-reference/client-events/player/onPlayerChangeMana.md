---
title: 'onPlayerChangeMana'
---
# `event` onPlayerChangeMana <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered when player mana changes.

## Parameters
```c++
int playerid, int previous, int current
```

* `int` **playerid**: the id of the player whose health points gets changed.
* `int` **previous**: the previous mana points of the player.
* `int` **current**: the current mana points of the player.

