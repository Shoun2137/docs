---
title: 'onPlayerShoot'
---
# `event` onPlayerShoot <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered when player shoots using ranged weapon.

## Parameters
```c++
int playerid, Item munition
```

* `int` **playerid**: the id of the shooting player.
* `Item` **munition**: the item object. For more information see [Item class](../../../client-classes/game/Item/).

