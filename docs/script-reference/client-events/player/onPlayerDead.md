---
title: 'onPlayerDead'
---
# `event` onPlayerDead <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.2"

This event is triggered when a player or npc dies.

## Parameters
```c++
int id
```

* `int` **id**: the id of the player who died.

