---
title: 'onPlayerDestroy'
---
# `event` onPlayerDestroy <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player disconnects from the server.

## Parameters
```c++
int id
```

* `int` **id**: the id of disconnected player.

