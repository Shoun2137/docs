---
title: 'onPlayerChangeColor'
---
# `event` onPlayerChangeColor <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This event is triggered when player changed his nickname color.

## Parameters
```c++
int id, int r, int g, int b
```

* `int` **id**: the id of the player who changed his nickname color.
* `int` **r**: the amount of red in the nickname color `(0 - 255)`.
* `int` **g**: the amount of green in the nickname color `(0 - 255)`.
* `int` **b**: the amount of blue in the nickname color `(0 - 255)`.

