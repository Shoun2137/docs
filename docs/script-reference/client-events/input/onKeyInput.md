---
title: 'onKeyInput'
---
# `event` onKeyInput <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Certain key buttons can produce a whitespace character, here's the list of them:<br/> **KEY_ESCAPE** = `'\x1b'`<br/> **KEY_BACK** = `'\b'`<br/> **KEY_RETURN** = `'\r'`<br/> **KEY_TAB** = `'\t'`
!!! note
    Key combinations like (CTRL+KEY) will produce a ASCII control character, See [ASCII table](http://www.ftj.agh.edu.pl/complab/doc/Cisco_IOS_11.0/xbook/xapascii.htm) for more details.

This event is triggered when user presses a button on their keyboard that produces character.

## Parameters
```c++
int key, int character
```

* `int` **key**: pressed key by user. See [Key constants](../../../client-constants/key/) for more details.
* `int` **character**: typed character by user.

