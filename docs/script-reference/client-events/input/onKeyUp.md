---
title: 'onKeyUp'
---
# `event` onKeyUp <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when user releases any button on their keyboard, mouse or joystick.

## Parameters
```c++
int key
```

* `int` **key**: released key by user. See [Key constants](../../../client-constants/key/), [Mouse constants](../../../client-constants/mouse/) for more details.

