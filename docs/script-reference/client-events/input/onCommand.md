---
title: 'onCommand'
---
# `event` onCommand <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"
!!! note
    This event can be cancelled in order to prevent server-side "onPlayerCommand" event from being called.

This event is triggered when a user uses command on the chat.  
Command always begins with forward slash `/`.

## Parameters
```c++
string command, string params
```

* `string` **command**: used command name on the chat.
* `string` **params**: command parameters divided by space.

