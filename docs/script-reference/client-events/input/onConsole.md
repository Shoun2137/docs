---
title: 'onConsole'
---
# `event` onConsole <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    By default, non multiplayer commands will cause an error `command doesn't exists`, in order to avoid it, call [eventValue](../../../shared-functions/event/eventValue/) function with [CONSOLE_COMMAND_FOUND](../../../client-constants/console/)

This event is triggered when a user uses command on the debug console.  
The console shows up when user presses the tilde key `~`.

## Parameters
```c++
string command, string params
```

* `string` **command**: used command name on the debug console.
* `string` **params**: command parameters divided by space.

