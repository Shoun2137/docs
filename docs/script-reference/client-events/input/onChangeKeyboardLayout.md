---
title: 'onChangeKeyboardLayout'
---
# `event` onChangeKeyboardLayout <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when user changes the keyboard layout.

## Parameters
No parameters.

