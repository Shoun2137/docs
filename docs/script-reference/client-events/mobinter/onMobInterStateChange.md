---
title: 'onMobInterStateChange'
---
# `event` onMobInterStateChange <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when hero/npc changes animation state while interacting with mob inter object in the world.
In Gothic, mob inters are special vobs on the map, that hero/npc can interact with. For example bed, door, chest etc.

## Parameters
```c++
int playerid, userdata address, int type, int from, int to
```

* `int` **playerid**: the id of the hero/npc that is interacting with interaction mob. If value is `-1`, `rewind` animation is being played without hero/npc interaction.
* `userdata` **address**: the address of interaction mob.
* `int` **type**: the type of interacted mob. For more information see [Vob types](../../../client-constants/vob/).
* `int` **from**: represents previous state of mob. If value is `1`, then mob was used, in any other case value is `0`.
* `int` **to**: represents current state of mob. If value is `1`, then mob is used, in any other case value is `0`.

