---
title: 'Skill weapon'
---
# `constants` Skill weapon <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `WEAPON_1H` | Represents npc one handed weapon skill. |
| `WEAPON_2H` | Represents npc two handed weapon skill. |
| `WEAPON_BOW` | Represents npc bow weapon skill. |
| `WEAPON_CBOW` | Represents npc crossbow weapon skill. |
