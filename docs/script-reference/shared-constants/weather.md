---
title: 'Weather'
---
# `constants` Weather <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `WEATHER_SNOW` | Represents snowing weather type. |
| `WEATHER_RAIN` | Represents raining weather type. |
