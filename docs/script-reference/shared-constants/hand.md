---
title: 'Hand'
---
# `constants` Hand <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `HAND_LEFT` | Represents npc left hand slot id. |
| `HAND_RIGHT` | Represents npc right hand slot id. |
