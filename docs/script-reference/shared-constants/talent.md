---
title: 'Talent'
---
# `constants` Talent <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `TALENT_SNEAK` | Represents npc sneak talent. |
| `TALENT_PICK_LOCKS` | Represents npc picklock talent. |
| `TALENT_PICKPOCKET` | Represents npc pickpocket talent. |
| `TALENT_RUNES` | Represents npc runes creation talent. |
| `TALENT_ALCHEMY` | Represents npc potion creation talent. |
| `TALENT_SMITH` | Represents npc smith talent. |
| `TALENT_THROPHY` | Represents npc trophy gathering talent. |
| `TALENT_ACROBATIC` | Represents npc acrobatic talent. |
| `TALENT_REGENERATE` | Represents npc health regeneration talent. |
| `TALENT_FIREMASTER` | Represents npc firemaster talent (unused by the game). |
| `TALENT_PICKPOCKET_UNUSED` | Represents npc old pickpocket talent (unused by the game). |
| `TALENT_A` | Represents npc talent A (unused by the game). |
| `TALENT_B` | Represents npc talent B (unused by the game). |
| `TALENT_C` | Represents npc talent C (unused by the game). |
| `TALENT_D` | Represents npc talent D (unused by the game). |
| `TALENT_E` | Represents npc talent E (unused by the game). |
