---
title: 'Damage'
---
# `constants` Damage <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `DAMAGE_UNKNOWN` | Represents unknown damage type. |
| `DAMAGE_BARRIER` | Represents barrier damage type. |
| `DAMAGE_BLUNT` | Represents blunt (weapon) damage type. |
| `DAMAGE_EDGE` | Represents edge/sharp (weapon) damage type. |
| `DAMAGE_FIRE` | Represents fire damage type. |
| `DAMAGE_FLY` | Represents fly damage type. |
| `DAMAGE_MAGIC` | Represents magic damage type. |
| `DAMAGE_POINT` | Represents point (weapon) damage type. |
| `DAMAGE_FALL` | Represents fall damage type. |
