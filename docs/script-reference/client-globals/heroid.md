---
title: 'heroId'
---
# `int` heroId <font size="4">(client-side)</font>

Represents the client player id.  
Use it, to manipulate the hero (local player) with player functions, e.g: [setPlayerPosition](../../client-functions/player/setPlayerPosition/).