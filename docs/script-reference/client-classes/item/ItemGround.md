---
title: 'ItemGround'
---
# `static class` ItemGround <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents item on the ground.


## Properties
### `int` id <font size="2">(read-only)</font>

Represents the unique id of the item ground.

----
### `Item&` item <font size="2">(read-only)</font>

Represents the item game object.

----

## Methods
No methods.

----
