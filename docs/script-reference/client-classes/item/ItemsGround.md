---
title: 'ItemsGround'
---
# `static class` ItemsGround <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents item ground manager.


## Properties
No properties.

----

## Methods
### getById
!!! note
    the returned object is always a new instance, to compare them, compare their ids.

This method will retrieve the item ground object by its unique id.

```cpp
ItemGround getById(int itemGroundId)
```

**Parameters:**

* `int` **itemGroundId**: the unique item ground id.
  
**Returns `ItemGround`:**

the item ground object or `null` if the object cannot be found.

----
### getByItem
!!! note
    the returned object is always a new instance, to compare them, compare their ids.

This method will retrieve the item ground object by item instance.

```cpp
ItemGround getByItem(Item item)
```

**Parameters:**

* `Item` **item**: the game object representing item.
  
**Returns `ItemGround`:**

the item ground object or `null` if the object cannot be found.

----
