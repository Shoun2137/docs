---
title: 'Item'
---
# `class` Item `extends` [Vob](../Vob/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents game item, which can be placed in the world or inventory.

### Constructor
```cpp
Item(string instance)
```

**Parameters:**

* `string` **instance**: the item instance from Daedalus scripts.
### Constructor
```cpp
Item(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the item from the game.

## Properties
### `string` instance 

Represents the instance of the item instance from Daedalus scripts.

----
### `int` idx 

Represents the unique ID of the item (unused by the game).

----
### `string` name 

Represents the name of the item.

----
### `string` nameID 

Unused field by the game.

----
### `int` hitp 

Unused field by the game.

----
### `int` max_hitp 

Unused field by the game.

----
### `int` mainflag 

Represents the bitflag that describes the item category.

----
### `int` flags 

Represents the bitflag that describes the item sub-category & state.

----
### `int` weight 

Represents the weight of the item (unused by the game).

----
### `int` value 

Represents the value of the item.

----
### `int` damageTypes 

Represents the bitflag that describes the type of damage the item can inflict (applicable to weapons).

----
### `int` wear 

Represents how the item can be worn (applicable to armors).

----
### `int` nutrition 

Represents the healing value of the item (applicable to food).

----
### `int` magic 

Unused field by the game.

----
### `int` owner 

Represents the daedalus symbol index of the npc which owns the item (used by Game Scripts).

----
### `int` ownerGuild 

Represents the ID of the guild which owns the item (used by Game Scripts).

----
### `int` disguiseGuild 

Represents the ID of the guild which the item will disguise player as.

----
### `string` file 

Represents the file name of the item model.

----
### `string` visual_change 

Represents the file name of the .ASC model applied to player (applicable to armors).

----
### `string` effectName 

Represents the VFX instance of the effect rendered on the item.

----
### `int` visual_skin 

Represents the texture variation ID used by the model defined in `visual_change` property (applicable to armors).

----
### `string` scemeName 

Represents the possible state animation, which will be played by player on interaction with the item.

----
### `int` material 

Represents the material type which will determine the sound of the item related actions (collisions, weapon draw, etc).

----
### `int` munition 

Represents the daedalus symbol index of the munition item required by the item (applicable to ranged weapons).

----
### `int` spell 

Represents the daedalus symbol index of the spell casted by the item (applicable to runes & scrolls).

----
### `int` range 

Represents the range of the item (applicable to weapons).

----
### `int` mag_circle 

Represents the magic circle required by the item.

----
### `string` description 

Represents the description of the item.

----
### `int` inv_zbias 

Represents the z bias (Depth Bias) of rendered item model.

----
### `int` inv_rotx 

Represents the rotation of the rendered item on the x axis.

----
### `int` inv_roty 

Represents the rotation of the rendered item on the y axis.

----
### `int` inv_rotz 

Represents the rotation of the rendered item on the z axis.

----
### `int` inv_animate 

Represents the animation state of the rendered item.

----
### `int` amount 

Represents the amount of the item.

----

## Methods
### getDamage

This method will get the item damage dealt by the specified damage type.

```cpp
int getDamage(int damageType)
```

**Parameters:**

* `int` **damageType**: the damage type (in range between [0, 7]).
  
**Returns `int`:**



----
### setDamage

This method will set the item damage dealth by the specified damage type.

```cpp
void setDamage(int damageType, int damage)
```

**Parameters:**

* `int` **damageType**: the damage type (in range between [0, 7]).
* `int` **damage**: the damage dealt.
  

----
### getProtection

This method will get the protection offered by the item against the specified damage type.

```cpp
int getProtection(int damageType)
```

**Parameters:**

* `int` **damageType**: the damage type (in range between [0, 7]).
  
**Returns `int`:**



----
### setProtection

This method will set the protection offered by the item against the specified damage type.

```cpp
void setProtection(int damageType, int protection)
```

**Parameters:**

* `int` **damageType**: the damage type (in range between [0, 7]).
* `int` **protection**: the protection offered.
  

----
### getCondAtr

This method will get the attribute type required to use the item.

```cpp
int getCondAtr(int index)
```

**Parameters:**

* `int` **index**: the index of the attribute in the condition list (in range between [0, 2]).
  
**Returns `int`:**



----
### setCondAtr

This method will set the attribute type required to use the item.

```cpp
void setCondAtr(int index, int attributeType)
```

**Parameters:**

* `int` **index**: the index of the attribute in the condition list (in range between [0, 2]).
* `int` **attributeType**: the attribute type required.
  

----
### getCondValue

This method will get the attribute value required to use the item.

```cpp
int getCondValue(int index)
```

**Parameters:**

* `int` **index**: the index of the value in the condition list (in range between [0, 2]).
  
**Returns `int`:**



----
### setCondValue

This method will set the attribute value required to use the item.

```cpp
void setCondValue(int index, int value)
```

**Parameters:**

* `int` **index**: the index of the value in the condition list (in range between [0, 2]).
* `int` **value**: the value required.
  

----
### getChangeAtr

This method will get the attribute type of bonus added by using the item.

```cpp
int getChangeAtr(int index)
```

**Parameters:**

* `int` **index**: the index of the attribute in the bonus list (in range between [0, 2]).
  
**Returns `int`:**



----
### setChangeAtr

This method will set the attribute type of bonus added by using the item.

```cpp
void setChangeAtr(int index, int attributeType)
```

**Parameters:**

* `int` **index**: the index of the attribute in the bonus list (in range between [0, 2]).
* `int` **attributeType**: the attribute type of the bonus.
  

----
### getChangeValue

This method will get the attribute value of bonus added by using the item.

```cpp
int getChangeValue(int index)
```

**Parameters:**

* `int` **index**: the index of the value in the bonus list (in range between [0, 2]).
  
**Returns `int`:**



----
### setChangeValue

This method will set the attribute value of bonus added by using the item.

```cpp
void setChangeValue(int index, int value)
```

**Parameters:**

* `int` **index**: the index of the value in the bonus list (in range between [0, 2]).
* `int` **value**: the value of the bonus.
  

----
### getText

This method will get the text at specified line that will be shown in inventory when selecting the item.

```cpp
int getText(int index)
```

**Parameters:**

* `int` **index**: the index of the text line (in range between [0, 5]).
  
**Returns `int`:**



----
### setText

This method will set the text at specified line that will be shown in inventory when selecting the item.

```cpp
void setText(int index, string the)
```

**Parameters:**

* `int` **index**: the index of the text line (in range between [0, 5]).
* `string` **the**: line of text that will be set.
  

----
### getCount

This method will get the count at specified line that will be shown in inventory when selecting the item.

```cpp
int getCount(int index)
```

**Parameters:**

* `int` **index**: the index of the text line (in range between [0, 5]).
  
**Returns `int`:**



----
### setCount

This method will set the count at specified line that will be shown in inventory when selecting the item.

```cpp
void setCount(int index, int the)
```

**Parameters:**

* `int` **index**: the index of the text line (in range between [0, 5]).
* `int` **the**: count that will be set.
  

----
