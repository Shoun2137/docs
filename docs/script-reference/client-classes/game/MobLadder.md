---
title: 'MobLadder'
---
# `class` MobLadder `extends` [MobInter](../MobInter/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents ladder object in the world.

### Constructor
```cpp
MobLadder(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
MobLadder(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
No properties.

----

## Methods
No methods.

----
