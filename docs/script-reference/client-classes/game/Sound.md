---
title: 'Sound'
---
# `class` Sound <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.2"

This class represents 2d sound.

### Constructor
```cpp
Sound(string fileName)
```

**Parameters:**

* `string` **fileName**: the name of the WAV audio file or SFX script instance.

## Properties
### `string` file 
!!! info "Available since version: 0.2"

Represents the file name of played sound or the name of SFX script instance.

----
### `float` playingTime <font size="2">(read-only)</font>

Represents the sound playing time in miliseconds.

----
### `float` volume 

Represents the sound volume scale in range 0.0 to 1.0.  
Setting it to `-1` will set the volume to the default value (which is either volume value from sfx script instance or 1.0).  
Setting it to to value lesser than `0.0` will cause the sound to be stopped.

----
### `bool` looping 

Represents the sound looping feature.

----
### `float` balance 

Represents the sound balance between left speaker and right speaker in range -1.0 to 1.0.  
Settting it to `-2` will set the balance to the default value (center).

----

## Methods
### play

This method will play the sound.

```cpp
void play()
```

  

----
### stop

This method will stop the sound.

```cpp
void stop()
```

  

----
### isPlaying
!!! info "Available since version: 0.1.10"

This method is used to check whether the sound is currently playing.

```cpp
bool isPlaying()
```

  
**Returns `bool`:**

`true` if the sound is currently playing, otherwise `false`.

----
