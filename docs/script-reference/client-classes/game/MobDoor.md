---
title: 'MobDoor'
---
# `class` MobDoor `extends` [MobLockable](../MobLockable/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This class represents door object in the world.

### Constructor
```cpp
MobDoor(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
MobDoor(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
No properties.

----

## Methods
No methods.

----
