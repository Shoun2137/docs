---
title: 'Sound3d'
---
# `class` Sound3d <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.2"

This class represents 3d sound.

### Constructor
```cpp
Sound3d(string fileName)
```

**Parameters:**

* `string` **fileName**: the name of the WAV audio file or SFX script instance.

## Properties
### `float` volume 
!!! info "Available since version: 0.1.10"

Represents the sound 3d volume scale in range 0.0 to 1.0.
Setting it to `-1` will set the volume to the default value (which is either volume value from sfx script instance or 0.5 approx).

----
### `bool` looping 
!!! info "Available since version: 0.1.10"

Represents the sound 3d looping feature.

----
### `string` file 
!!! info "Available since version: 0.2"

Represents the file name of played sound or the name of SFX script instance.

----
### `float` playingTime <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the sound 3d playing time in miliseconds.

----
### `float` obstruction 

Represents the sound 3d obstruction.

----
### `float` radius 

Represents the sound 3d radius.  
Settting it to `-1` will set the radius to the default value (3500).

----
### `float` coneAngle 

Represents the sound 3d cone angle in range 0.0 to 360.0.

----
### `float` reverbLevel 

Represents the sound 3d reverb level.

----
### `bool` ambient 

Represents the sound 3d ambient mode feature.

----
### `float` pitchOffset 

Represents the sound 3d pitch offset.

----

## Methods
### play

This method will play the 3d sound.

```cpp
void play()
```

  

----
### stop
!!! info "Available since version: 0.1.10"

This method will stop the 3d sound.

```cpp
void stop()
```

  

----
### isPlaying
!!! info "Available since version: 0.1.10"

This method is used to check whether the 3d sound is currently playing.

```cpp
bool isPlaying()
```

  
**Returns `bool`:**

`true` if the 3d sound is currently playing, otherwise `false`.

----
### setTargetVob

This method will set the target vob, from which position the 3d sound will be played.

```cpp
void setTargetVob(Vob vob)
```

**Parameters:**

* `Vob` **vob**: the vob object reference.
  

----
### setTargetPlayer

This method will set the target player/npc, from which position the 3d sound will be played.

```cpp
void setTargetPlayer(int playerid)
```

**Parameters:**

* `int` **playerid**: the id of the target player.
  

----
