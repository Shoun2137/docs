---
title: 'TraceRayReport'
---
# `class` TraceRayReport <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents trace ray result.


## Properties
### `userdata` vob <font size="2">(read-only)</font>

Represents found vob pointer in ray cast call.

----
### `Vec3` intersect 

Represents found intersection point in ray cast call.

----
### `Vec3` normal 

Represents found intersection point normals in ray cast call.

----

## Methods
No methods.

----
