---
title: 'BBox3d'
---
# `class` BBox3d <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents bounding box 3d.


## Properties
### `Vec3` mins 

Represents bottom corner position of bounding box.

----
### `Vec3` maxs 

Represents upper corner position of bounding box.

----
### `Vec3` center <font size="2">(read-only)</font>

Represents center of bounding box.

----

## Methods
### intersecting

This method will check if two bbox intersecting between each other.

```cpp
bool intersecting(BBox3d the)
```

**Parameters:**

* `BBox3d` **the**: bounding box 3d used to check intersection.
  
**Returns `bool`:**

`true` if two bounding box are intersecting, otherwise `false`.

----
