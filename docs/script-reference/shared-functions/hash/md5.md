---
title: 'md5'
---
# `function` md5 <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.0"

This function will calculate the MD5 hash of the specified string and return it as hexadecimal text representation.

## Declaration
```cpp
string md5(string input)
```

## Parameters
* `string` **input**: the string to hash.
  
## Returns `string`
Returns string hash as hexadecimal text representation.

