---
title: 'cancelEvent'
---
# `function` cancelEvent <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    Use this function inside a handler/function bound to event.

This function will cancel event.  
Cancelling G2O event can cause something will not occur (like game inventory won't open), you can also program your own custom logic using event cancellation.

## Declaration
```cpp
void cancelEvent()
```

## Parameters
No parameters.
  

