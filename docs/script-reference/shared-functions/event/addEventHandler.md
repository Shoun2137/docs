---
title: 'addEventHandler'
---
# `function` addEventHandler <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will bind function to specified event.

## Declaration
```cpp
bool addEventHandler(string eventName, function func, int priority = 9999)
```

## Parameters
* `string` **eventName**: the name of the event.
* `function` **func**: the reference to a function, keep in mind that function must have the same amount of arguments as event.
* `int` **priority**: the function priority. The lower the value, the sooner the function/handler will be called.
  
## Returns `bool`
`true` if specified event evists and operation was successfull, otherwise `false`.

