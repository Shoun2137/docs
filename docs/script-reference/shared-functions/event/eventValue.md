---
title: 'eventValue'
---
# `function` eventValue <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    Use this function inside a handler/function bound to event.

This function will set the event value.  
Setting an event value inside G2O event can modify something, like damage done in onPlayerHit event.

## Declaration
```cpp
void eventValue(int eventValue)
```

## Parameters
* `int` **eventValue**: the new event value.
  

