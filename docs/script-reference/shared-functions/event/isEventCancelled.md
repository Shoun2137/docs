---
title: 'isEventCancelled'
---
# `function` isEventCancelled <font size="4">(shared-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    Use this function inside a handler/function bound to event.

This function will check if the event was cancelled inside a handler bound to a event.

## Declaration
```cpp
bool isEventCancelled()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` if the event was cancelled inside a function bound to a event, otherwise `false`.

