---
title: 'blob.writestring'
---
# `function` blob.writestring <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This method will write a string into the blob.

## Declaration
```cpp
void blob.writestring(string string)
```

## Parameters
* `string` **string**: any string
  

