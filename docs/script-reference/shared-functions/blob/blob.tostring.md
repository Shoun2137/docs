---
title: 'blob.tostring'
---
# `function` blob.tostring <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This method will create string from blob.

## Declaration
```cpp
string blob.tostring()
```

## Parameters
No parameters.
  
## Returns `string`
Returns the blob as string.

