---
title: 'getTickCount'
---
# `function` getTickCount <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the amount of time that your system has been running in milliseconds.  
You can use this function to compare how long something would take to run.

## Declaration
```cpp
int getTickCount()
```

## Parameters
No parameters.
  
## Returns `int`
The amount of time that your system has been running in miliseconds.

