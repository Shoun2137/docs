---
title: 'getHostname'
---
# `function` getHostname <font size="4">(shared-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the hostname of the server.

## Declaration
```cpp
string getHostname()
```

## Parameters
No parameters.
  
## Returns `string`
Server hostname.

