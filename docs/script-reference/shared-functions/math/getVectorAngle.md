---
title: 'getVectorAngle'
---
# `function` getVectorAngle <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will get angle on Y axis directed towards the second point.

## Declaration
```cpp
float getVectorAngle(float x1, float y1, float x2, float y2)
```

## Parameters
* `float` **x1**: the position on X axis of the first point.
* `float` **y1**: the position on Y axis of the first point.
* `float` **x2**: the position on X axis of the second point.
* `float` **y2**: the position on Y axis of the second point.
  
## Returns `float`
Returns the angle on Y axis directed towards the second point.

