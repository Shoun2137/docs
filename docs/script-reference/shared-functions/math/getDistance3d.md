---
title: 'getDistance3d'
---
# `function` getDistance3d <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the 3d distance between two points.

## Declaration
```cpp
float getDistance3d(float x1, float y1, float z1, float x2, float y2, float z2)
```

## Parameters
* `float` **x1**: the position on X axis of the first point.
* `float` **y1**: the position on Y axis of the first point.
* `float` **z1**: the position on Z axis of the first point.
* `float` **x2**: the position on X axis of the second point.
* `float` **y2**: the position on Y axis of the second point.
* `float` **z2**: the position on Z axis of the second point.
  
## Returns `float`
Returns the calculated 3d distance between two points as floating point number.

