List of commands that can be used in client-side console.
Some commands can be useful to debug resources used by your server.
Sever owners can also register their own custom commands.

Commands listed below can be used with the client console, which can be open using **~** (*tilde*).

#### view *[name]*
Changes console tab to either `error` or `info`.
Information view show generic logs from multiplayer and server resources.
Error view show detailed information about errors from scripts.

Usage: `view <info|error>`

#### showfocus *[type]*
Enables or disables showing names of certain focus types.
If type is not provided, every focus type is disabled/enabled.

Usage: `view <item|mob|npc|bar>`

#### time
Prints current system time in-game console.

Usage: `time`

#### version
Prints current game client version.

Usage: `version`

#### fps [limit]
Setup upper fps limit for game client.

Usage: `fps <number>`

#### onscreendebug
Enables or disables showing debug information on screen.
While enabled information about used resources by game client and scripts will be shown.

Usage: `onscreendebug`

#### gc
Runs Squirrel garbage collector and returns the number of reference cycles found (and deleted).

Usage: `gc`

#### generate instances
> **NOTE!** This commands works only in debug mode.

Generates `instances.xml` file required by server, and it's located at `Gothic II\Multiplayer`.
File contains dumped information from **GOTHIC.DAT** about NPCs and Items.

Usage: `generate instances`
