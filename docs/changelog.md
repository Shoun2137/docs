**[BREAKING-CHANGE][REMOVED]** Shared class Items, use `Daedalus` class instead.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `equipArmor`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `unequipArmor`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `equipMeleeWeapon`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `unequipMeleeWeapon`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `equipRangedWeapon`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `unequipRangedWeapon`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `equipHelmet`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `unequipHelmet`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `equipShield`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side function `unequipShield`.  
**[BREAKING-CHANGE][REMOVED]** Server-side function `equipSpell`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side `startFaceAni`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side `getFaceAniName`.  
**[BREAKING-CHANGE][REMOVED]** Client-side and server-side `getPlayerFaceAni`.  
**[BREAKING-CHANGE][REMOVED]** Server-side event `onPlayerChangeFaceAni`.  
**[BREAKING-CHANGE][REMOVED]** Configuration node `face-ani` used to control face animations synchronization.  
**[BREAKING-CHANGE][REMOVED]** Client-side class `Line`.  
**[BREAKING-CHANGE][REMOVED]** Client-side class `Line2d`.  
**[BREAKING-CHANGE][REMOVED]** Client-side class `Line3d`.  
**[BREAKING-CHANGE][REMOVED]** Client-side function `getKeyLetter`.  
**[BREAKING-CHANGE][REMOVED]** Client-side function `getKeyLayout`.  
**[BREAKING-CHANGE][REMOVED]** Client-side function `setKeyLayout`.  
**[BREAKING-CHANGE][RENAMED]** Client-side event `onKey`, use `onKeyDown` instead.  
**[BREAKING-CHANGE][REMOVED]** Client-side event `onMobOpen`, use `onMobLockableOpen` instead.  
**[BREAKING-CHANGE][REMOVED]** Client-side event `onMobClose`, use `onMobLockableClose` instead.  
**[BREAKING-CHANGE][REMOVED]** Client-side event `onMobInteract`, use `onMobInterStateChange` instead.  
**[BREAKING-CHANGE][REMOVED]** Client-side event onRespawn.  
**[BREAKING-CHANGE][REMOVED]** Server-side function `getPlayerUID`. UIDs are no longer generated, due to many problems with its generation.  

**[BREAKING-CHANGE][REMOVED]** Player ID is no longer available from `oCNpc::idx` variable.  
**[BREAKING-CHANGE][UPDATED]** Replaced `items.xml` file with `instances.xml`, this file can also be generated via client-side command `generate instances`.  
**[BREAKING-CHANGE][UPDATED]** Client-side and server-side function `equipItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side and server-side function `unequipItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side and server-side function `useItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side and server-side function `useItemToState` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side and server-side function `giveItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side and server-side function `removeItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side function `getPlayerSpellNr` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side function `hasItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side event `onPlayerSpellSetup` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side event `onPlayerSpellCast` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Client-side event `onPlayerUseItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipArmor` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipMeleeWeapon` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipRangedWeapon` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipShield` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipHelmet` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipHandItem` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipAmulet` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipRing` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipBelt` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerEquipSpell` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerSpellSetup` parameter `item_id` (int) changed to `item_instance` (string).  
**[BREAKING-CHANGE][UPDATED]** Server-side event `onPlayerSpellCast` parameter `item_id` (int) changed to `item_instance` (string).  

**[FIXED]** Internal time inaccuracies that could lead to imprecise execution of the timer.  
**[FIXED]** Some of the unsynced items weren't removed from the world during load.  
**[FIXED]** `gameAbnormalExit` will be set to `0` while pressing *CTRL+C* in debug external console.  
**[FIXED]** Client-side and server-side function `sscanf`. When passing spaces, the conversion to decimal or float arguments was passing and the arguments had odd values. Now the invalid conversion will return `null`.  
**[FIXED]** Server-side function `findNearbyPlayers`. There was a problem with invalid bounding box generation used for lookup.  
**[FIXED]** Client-side event `onUnequip` not being called for torches, rings, amulets and belts.  
**[FIXED]** Client-side event `onMouseMove` swapped arguments, now `x` will represent the `x` axis movement and `y` will represent the `y` axis movement.  
**[FIXED]** Client-side event `onRenderFocus` not working properly when target has empty name.  

**[UPDATED]** Increased health and mana limit to `16777215`.  
**[UPDATED]** New method of player serial generation, for more secure and stable bans.  
**[UPDATED]** Client-side constants `HUD_*` values were changed to bit fields.  
**[UPDATED]** Client-side function `setHudMode` now support bit fields.  
**[UPDATED]** Client-side event `onPaste` will now be triggered even if chat input isn't opened.  
**[UPDATED]** Client-side events `onPlayerSpawn` and `onPlayerUnspawn` are also called now for local npcs.  

**[ADDED]** Support for 32-bit **BGRA8888** texture format. Please note, that Gothic natively not support auto conversion from this format to `.TEX` (compiled). Use external tools like **zTEXiPy** to compile texture.  
**[ADDED]** Configuration node `<tickrate-limit>60</tickrate-limit>` to limit server ticks per second in order to safe some CPU cycles. Default value is `60`.  
**[ADDED]** Built-in streamer for local NPCs. Default local NPCs streamer range is `5000`.  
**[ADDED]** `instances.xml` file, which contains detailed information about NPCs and Items from `Gothic.DAT`.  
**[ADDED]** Client-side console command `generate instances` for generating `Gothic II/Multiplayer/instances.xml` file that will contain items and npc instances required by server.  
**[ADDED]** Field `bitsUsed` for shared class `Packet`.  
**[ADDED]** Field `bytesUsed` for shared class `Packet`.  
**[ADDED]** Optional argument `aniName` for client-side and server-side function `stopFaceAni`.  
**[ADDED]** Client-side and server-side function `getPlayerFaceAnis`.  
**[ADDED]** Server-side class `Daedalus` for managing and querying information from `instances.xml` file.  
**[ADDED]** Server-side method `Daedalus.symbol`.  
**[ADDED]** Server-side method `Daedalus.instance`.  
**[ADDED]** Client-side and server-side method `Daedalus.index`.  
**[ADDED]** Server-side NPCs synchronization.  
**[ADDED]** Server-side function `getPlayerWaterLevel`.  
**[ADDED]** Server-side function `drawWeapon`.  
**[ADDED]** Server-side function `removeWeapon`.  
**[ADDED]** Server-side function `readySpell`.  
**[ADDED]** Server-side function `unreadySpell`.  
**[ADDED]** Server-side function `hitPlayer`.  
**[ADDED]** Server-side function `createNpc`.  
**[ADDED]** Server-side function `destroyNpc`.  
**[ADDED]** Server-side function `getNpcHostPlayer`.  
**[ADDED]** Server-side function `getNpcLastCommandId`.  
**[ADDED]** Server-side function `isNpc`.  
**[ADDED]** Server-side function `npcClearQueue`.  
**[ADDED]** Server-side function `npcAttackMelee`.  
**[ADDED]** Server-side function `npcAttackRanged`.  
**[ADDED]** Server-side function `npcSpellCast`.  
**[ADDED]** Server-side event `onNPCCreated`.  
**[ADDED]** Server-side event `onNPCDestroyed`.  
**[ADDED]** Server-side event `onNPCChangeHostPlayer`.  
**[ADDED]** Server-side event `onNPCCommandFinished`.  
**[ADDED]** Server-side event `onPlayerToggleFaceAni`.  
**[ADDED]** Patch for `oCGame::SetHeroStatus` (Note! Music theme is now fully managed by the scripts, so `THR` theme won't be set when portal has name "~WALD~" or when hero has less or equal health points to 5).  
**[ADDED]** Client-side constants `HERO_STATUS_STD`, `HERO_STATUS_THR` and `HERO_STATUS_FGT`.  
**[ADDED]** Client-side function `getHeroStatus`.  
**[ADDED]** Client-side function `setHeroStatus`.  
**[ADDED]** Client-side function `drawLine`.  
**[ADDED]** Client-side function `drawLine3d`.  
**[ADDED]** Client-side function `disableHumanAI` for controlling hero movement control logic.  
**[ADDED]** Client-side function `getKeyboardCodePage`.  
**[ADDED]** Client-side function `getKeyboardLayout`.  
**[ADDED]** Client-side function `getKeyboardLocaleName`.  
**[ADDED]** Client-side function `getKeyboardLangName`.  
**[ADDED]** Client-side function `isLocalNpc`.  
**[ADDED]** Client-side function `isRemoteNpc`.  
**[ADDED]** Client-side function `isNpcHosted`.  
**[ADDED]** Client-side function `enablePlayerInterpolation`.  
**[ADDED]** Setter `matrix` for client-side class `Vob`, because dx11 doesn't like direct matrix manipulation.  
**[ADDED]** Client-side event `onPlayerRespawn`.  
**[ADDED]** Client-side event `onKeyUp`.  
**[ADDED]** Client-side event `onKeyDown`.  
**[ADDED]** Client-side event `onKeyInput`.  
**[ADDED]** Client-side event `onChangeKeyboardLayout`.  
**[ADDED]** Client-side event `onMobInterStartInteraction`.  
**[ADDED]** Client-side event `onMobInterStopInteraction`.  
**[ADDED]** Client-side event `onMobInterStateChange`.  
**[ADDED]** Client-side event `onPlayerInterrupt`.  
**[ADDED]** Client-side event `onMobLockableOpen`.  
**[ADDED]** Client-side event `onMobLockableClose`.  
**[ADDED]** Client-side event `onWindowFocus`.  
**[ADDED]** Client-side event `onPortalChange`.  
**[ADDED]** Client-side field `Vob.sectorName`.  

**[REMOVED]** Fast chat input clear while holding backspace for longer than 1500 milliseconds.  
